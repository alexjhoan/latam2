import React from 'react'
import {Row, Col, Button} from "antd";
import { Typography } from 'antd';
import Fade from 'react-reveal/Fade';
import { useTheme } from "../shared-components/Styles/ThemeHook";

export default function OpenDay() {
	const { Title } = Typography;
	const { theme } = useTheme();

	return (
		<div className='containerHotSale'>
			<Row className="containerLanding ">
				<Fade bottom>
					<Col xs={24}>
						<p className='title'>¡Cerramos LatAm Invierte con 7 propuestas imperdibles!</p>
						<p className="subTitle">Un lanzamiento en Carrasco, un proyecto para invertir en Asunción y 5 proyectos con hasta 15% de descuento en Montevideo.</p>
						<p className="strong">Mirá el video para conocer todas las propuestas de la mano de sus desarrolladores!</p>
					</Col>
				</Fade>
			</Row>
			<div className='containerDesktop'>
				<Row className="containerLanding">
					<Col xs={24} md={12} className='containerImgHot dMdFlex'>
						<Fade left>
							<img src="/images/hotSale.png" alt="hotSale" className='imgHotSale'/>
						</Fade>
					</Col>
					<Col xs={24} md={12}>
						<Fade right>
							<div className="theVideo">
								<iframe width="1090" height="613" src="https://www.youtube.com/embed/tewPnIMN25s"></iframe>
							</div>
						</Fade>
					</Col>
				</Row>
			</div>
			<style jsx>{`
				.containerHotSale :global(.containerLanding){
					padding: 15px;
				}
				.imgHotSale {
					max-width: 100%;
					max-height: 330px;
				}
				.title {
					font-size: 19px;
					font-weight: 700;
					line-height: 1.4;
					text-align: center;
					margin-bottom: 15px;
				}
				.subTitle {
					text-align: center;
					font-weight: 400;
					line-height: 1.3;
					font-size: 14px;
				}
				.strong {
					text-align: center;
					font-weight: 700;
					line-height: 1.3;
					font-size: 14px;
				}
				.theVideo{
					padding-top: 56.26%;
					position: relative;
					width: 100%;
				}
				.theVideo :global(iframe) {
					position: absolute;
					top: 0;
					left: 0;
					width: 100%;
					height: 100%;
				}
				.imgZoom {
					width: 25px;
					margin: 0 5px;
				}
				.singUp :global(.ant-btn) {
					padding: 5px 30px;
					font-size: 21px;
					height: auto;
				}
				.containerDesktop {
					background-color: #3a4145;
				}
				.containerDesktop :global(.containerImgHot) {
					display: flex;
					flex-direction: row;
					justify-content: center;
					align-items: flex-end;
					padding: 0 16px;
				}
				
				@media (min-width: ${theme.breakPoints.sm}){
					.title {
						font-size: 28px;
					}
					.subTitle {
						font-size: 18px;
						font-weight: 500;
					}
					.strong {
						font-weight: 700;
						font-size: 18px;
					}
				}
				@media (min-width: ${theme.breakPoints.md}){
					.containerHotSale :global(.containerLanding){
						padding: 15px 15px 0;
					}
					.title {
						text-align: left;
					}
					.subTitle {
						text-align: left;
						font-size: 20px;
						margin-bottom: 0px;
					}
					.strong {
						text-align: left;
						font-size: 20px;
					}
					.theVideo{
						padding-top: 56.26%;
						position: relative;
						width: 100%;
						margin: 4vh 0;
					}
					.imgZoom {
						width: 25px;
						margin: 0 5px;
					}
					.singUp :global(.ant-btn) {
						font-size: 24px;
					}
				}
				@media (min-width: ${theme.breakPoints.xxl}){
					.title {
						text-align: left;
						font-size: 28px;
					}
					.imgZoom {
						width: 30px;
						margin: 0 5px;
					}
					.singUp :global(.ant-btn) {
						font-size: 28px;
					}
				}
			`}</style>
		</div>
	)
}