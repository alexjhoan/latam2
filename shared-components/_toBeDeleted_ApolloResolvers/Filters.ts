import {
	APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST,
	APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST,
} from "./constants";
import { gql } from "@apollo/client";

// const _PERSIST_ = false;
const _PERSIST_ = false;
const _NAME_ = "Filters";

// operation_type_id: Int!
// property_type_id: [Int]
// estate_id: Int
// neighborhood_id: [Int]
// searchstring: String
// projects: Boolean
// ownerID: Int
// projectTitle: String
// withBase: Boolean
// auctionType: Int
// bedroomsID: [Int]
// bathroomsID: [Int]
// search_by_point_of_interest: [Int]
// overID: Int
// privateOwner: Int
// facilitiesGroup: [Int]
// security: Boolean
// seaview: Boolean
// order: Int
// socialHousing: Boolean
// constStatesID: [Int]
// floors: [Int]
// dispositionID: [Int]
// condominium: Boolean
// minPrice: Int
// maxPrice: Int
// currencyID: Int
// m2Min: Int
// m2Max: Int
// m2Type: String
// commonExpenses: Boolean
// publicationDate: Int
// allowMapInfo: Boolean
// mapCenter: String
// mapZoom: String
// allowlatlong: Boolean
// latlong: [String]
// groundUssageID: [Int]
// season: String
// dateFrom: String
// dateTo: String
// guests: Int
// seaDistanceID: Int
// map_polygon: [String]
// securityId: Int
// page: Int

const typename =
	(_PERSIST_
		? APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST
		: APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST) + _NAME_;

export const TYPENAME_GENERIC_FILTER = typename + "_" + "generic_filter";
const initialState = {
	operation_type_id: null,
	property_type_id: [],
	estate_id: null,
	neighborhood_id: [],
	publicationDate: null,
	searchstring: null,
	seaview: null,
	ownerID: null,
	projects: null,
	__typename: typename,
	projectTitle: null,
	withBase: null,
	auctionType: null,
	search_by_point_of_interest: [],
	overID: null,
	privateOwner: null,
	facilitiesGroup: [],
	security: null,
	order: {
		value: 2,
		text: "Popularidad",
		__typename: TYPENAME_GENERIC_FILTER,
	},
	socialHousing: null,
	constStatesID: [],
	floors: [],
	dispositionID: [],
	condominium: null,
	minPrice: null,
	maxPrice: null,
	currencyID: null,
	m2Min: null,
	m2Max: null,
	m2Type: null,
	commonExpenses: null,
	allowMapInfo: null,
	mapCenter: null,
	mapZoom: null,
	allowlatlong: null,
	latlong: null,
	groundUssageID: null,
	season: null,
	dateFrom: null,
	dateTo: null,
	guests: null,
	bathrooms: [],
	bedrooms: [],
	rooms: [],
	seaDistanceID: null,
	map_bounds: [],
	map_polygon: [],
	securityId: null,
	page: {
		value: 1,
		text: "Pagina 1",
		__typename: TYPENAME_GENERIC_FILTER,
	},
};

export const QUERY_FILTERS = gql`
    query QUERY_FILTERS{
        ${typename} @client  {
 			operation_type_id {
 				value
 				text
 				__typename
 			}
			property_type_id {
 				value
 				text
 				__typename
 			}
 			estate_id {
 				value
 				text
 				__typename
 			}
			neighborhood_id {
 				value
 				text
 				__typename
 			}
			publicationDate {
 				value
 				text
 				__typename
 			}
			searchstring {
 				value
 				text
 				__typename
 			}
			seaview {
 				value
 				text
 				__typename
 			}
			ownerID {
 				value
 				text
 				__typename
 			}
			projects {
 				value
 				text
 				__typename
 			}
			__typename
			projectTitle {
 				value
 				text
 				__typename
 			}
			withBase {
 				value
 				text
 				__typename
 			}
			auctionType {
 				value
 				text
 				__typename
 			}
			search_by_point_of_interest {
 				value
 				text
 				__typename
 			}
			overID {
 				value
 				text
 				__typename
 			}
			privateOwner {
 				value
 				text
 				__typename
 			}
			facilitiesGroup {
 				value
 				text
 				__typename
 			}
			security {
 				value
 				text
 				__typename
 			}
			order {
 				value
 				text
 				__typename
 			}
			socialHousing {
 				value
 				text
 				__typename
 			}
			constStatesID {
 				value
 				text
 				__typename
 			}
			floors {
 				value
 				text
 				__typename
 			}
			dispositionID {
 				value
 				text
 				__typename
 			}
			condominium {
 				value
 				text
 				__typename
 			}
			minPrice {
 				value
 				text
 				__typename
 			}
			maxPrice {
 				value
 				text
 				__typename
 			}
			currencyID {
 				value
 				text
 				__typename
 			}
			m2Min {
 				value
 				text
 				__typename
 			}
			m2Max {
 				value
 				text
 				__typename
 			}
			m2Type {
 				value
 				text
 				__typename
 			}
			commonExpenses {
 				value
 				text
 				__typename
 			}
			allowMapInfo {
 				value
 				text
 				__typename
 			}
			mapCenter {
 				value
 				text
 				__typename
 			}
			mapZoom {
 				value
 				text
 				__typename
 			}
			allowlatlong {
 				value
 				text
 				__typename
 			}
			latlong {
 				value
 				text
 				__typename
 			}
			groundUssageID {
 				value
 				text
 				__typename
 			}
			season {
 				value
 				text
 				__typename
 			}
			dateFrom {
 				value
 				text
 				__typename
 			}
			dateTo {
 				value
 				text
 				__typename
 			}
			guests {
 				value
 				text
 				__typename
 			}
			seaDistanceID {
 				value
 				text
 				__typename
 			}
			map_bounds {
 				value {
 				  latitude
 				  longitude
 				  __typename
 				}
 				text
 				__typename
 			}
			map_polygon {
 				value {
 				  latitude
 				  longitude
 				  __typename
 				}
 				text
 				__typename
 			}
			securityId {
 				value
 				text
 				__typename
 			}
			page {
 				value
 				text
 				__typename
 			}
			bathrooms {
 				value
 				text
 				__typename
 			}
			bedrooms {
 				value
 				text
 				__typename
 			}
			rooms {
 				value
 				text
 				__typename
 			}
        }
	}`;

export const TYPENAME_FILTERS = typename;
export default {
	typename,
	initialState: { [typename]: initialState },
	customQueries: {},
	mutations: {},
};
