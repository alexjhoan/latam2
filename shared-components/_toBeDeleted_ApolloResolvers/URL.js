import {gql} from "@apollo/client";
import {
  APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST,
  APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST,
} from "./constants";

const _PERSIST_ = false;
const _NAME_ = "CurrentUrl";

const typename =
  (_PERSIST_
    ? APOLLO_RESOLVER_MAGIC_KEYWORD_PERSIST
    : APOLLO_RESOLVER_MAGIC_KEYWORD_NO_PERSIST) + _NAME_;
const initialState = {
  url: null,
  __typename: typename,
};

export const QUERY_CURRENT_URL = gql`
    query QUERY_CURRENT_URL{
        ${typename} @client  {
            url
        }
    }
`;
export const TYPENAME_CURRENT_URL = typename;
export default {
  typename,
  initialState: { [typename]: initialState },
  customQueries: {},
  mutations: {},
};
