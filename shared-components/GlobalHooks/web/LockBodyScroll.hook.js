import { useEffect } from "react";

export function useLockBodyScroll(trigger) {
  useEffect(() => {
    if (trigger) {
      const originalStyle = window.getComputedStyle(document.body).overflow;
      document.body.style.overflow = "hidden";
      return () => (document.body.style.overflow = originalStyle);
    }
  }, [trigger]);
}
