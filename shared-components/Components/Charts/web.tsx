import ReactFrappeChart from "react-frappe-charts";
import React, { useEffect, useState } from "react";
import { Skeleton } from "antd";
import { useDebouncedCallback } from "../../GlobalHooks/useDebounce";
import { parseMoney } from "../../Utils/Functions";

interface ChartLineInterface {
	className: any;
	stroke?: {
		width?: number;
		color?: string;
	};
	labels: string[];
	datasets: {
		name: string;
		values: number[];
		colors?: string[];
	}[];
	trendline?: {
		label: string;
		value: number;
	};
	title?: string;
	height: any;
	currency?: any;
}

const ChartLine = ({
	className,
	title = "",
	stroke,
	labels = [],
	datasets = [],
	trendline,
	height,
	currency,
}: ChartLineInterface) => {
	const classNameKey = String(className).replace(/[^0-9A-Z]+/gi, "");
	const [isResizeing, setResizeing] = useState(false);
	const [debouncedSetResizeing] = useDebouncedCallback(
		(value: boolean) => setResizeing(value),
		1000
	);

	const resizeStart = () => {
		if (!isResizeing) {
			setResizeing(true);
		}
		debouncedSetResizeing(false);
	};

	useEffect(() => {
		window.addEventListener("resize", resizeStart);
		return () => {
			window.removeEventListener("resize", resizeStart);
		};
	}, []);

	const options = {
		data: {
			labels: labels,
			datasets: datasets,
			yMarkers: trendline ? [{ ...trendline, options: { labelPos: "left" } }] : null,
		},
		title: title,
		height: height,
		colors: stroke && stroke.color ? [stroke.color] : [],
		lineOptions: {
			dotSize: stroke?.width + 2,
			spline: 1,
		},
		formatLegendY: x => {
			return parseMoney(x);
		},
		tooltipOptions: {
			formatTooltipX: d => (d + "").toUpperCase(),
			formatTooltipY: d => (currency ? `${currency.name} ${parseMoney(d)}` : d),
		},
	};

	if (isResizeing) {
		return <Skeleton active />;
	}

	return (
		<>
			<div className={"react_frappe_chart_custom_" + classNameKey}>
				<ReactFrappeChart {...options} type={"line"} maxSlices={1999} />
			</div>
			<style jsx global>
				{`
					.react_frappe_chart_custom_${classNameKey} .line-graph-path {
						${stroke && stroke.width
							? `stroke-width: ${stroke.width}px !important`
							: ``};
					}
					.react_frappe_chart_custom_${classNameKey} .line-horizontal,
					.react_frappe_chart_custom_${classNameKey} .line-vertical {
						stroke-opacity: 40%;
					}
					.react_frappe_chart_custom_${classNameKey} .line-horizontal.dashed {
						stroke: ${stroke.color} !important;
						stroke-width: 3px;
						stroke-dasharray: 1px;
						stroke-opacity: 50%;
					}
					.graph-svg-tip {
						z-index: 8;
					}
				`}
			</style>
		</>
	);
};

export { ChartLine };
