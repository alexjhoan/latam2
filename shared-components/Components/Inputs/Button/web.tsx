import theme from "../../../Styles/_toBeDeleted_theme";
import React, { FC } from "react";
import { useButton, ButtonProps } from "./Button.hook";

export const Button: FC<ButtonProps> = (props: ButtonProps) => {
	const bStyles = useButton(props);

	return (
		<React.Fragment>
			<button
				className={`button ${props.type}  ${props.status ? props.status : ""}`}
				onClick={() => props.handleClick()}
				disabled={props.status == "disabled"}>
				{props.content}
			</button>
			<style jsx>{`
				.button {
					display: flex;
					flex: 1 1 auto;
					align-items: center;
					justify-content: center;
					font-size: ${theme.fontSizes.text}px;
					cursor: pointer;
					height: ${bStyles.height}; // with units
					width: ${bStyles.width}; // with units
					box-sizing: border-box;
					transition: all 300ms ease;
					border-radius: ${bStyles.radius}px; // with units
					color: ${bStyles.color};
					background: ${bStyles.background};
					padding: ${bStyles.paddingVertical}px ${bStyles.paddingHorizontal}px;
					border: ${bStyles.borderWith}px solid ${bStyles.borderColor};
					font-weight: bold;
					text-align: center;
					overflow: hidden;
					opacity: ${bStyles.opacity};
				}
				.button:not([disabled]):hover {
					background: ${bStyles.backgroundHover};
					color: ${bStyles.colorHover};
					cursor: pointer;
					border-color: ${bStyles.borderHover};
				}
				/*
				.readonly {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
				}
				.disabled {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
					opacity: 0.5;
				}
				*/
			`}</style>
		</React.Fragment>
	);
};
