import React, { ReactNode, FC } from "react";
import { Text, StyleSheet, View } from 'react-native';

export interface LabelProps {
  labelContent?: string;
  children?: ReactNode;
  className?: string;
  showLabel?: boolean;
  htmlID?: string;
}

export const Label: FC<LabelProps> = ({
  labelContent,
  children,
  showLabel = true,
}) => {
  return (
    <View>
      {showLabel && labelContent && (
        <Text>
          {labelContent}
        </Text>
      )}
      {children}
    </View>
  );
};
