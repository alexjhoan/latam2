import theme from "../../../../Styles/_toBeDeleted_theme";
import React, { FC } from "react";
import { SelectProps, useSelects } from "../Selects.hook";
import { Label } from "../../Label/web";

export interface NativeSelectProps extends SelectProps {
	type?: "single";
}

export const Select: FC<NativeSelectProps> = props => {
	const {
		nvalue,
		handleSelectOption,
		customKey,
		valueKey,
		placeholder,
		options,
		status,
	} = useSelects(props);

	return (
		<React.Fragment>
			<div className={"native-select" + (status ? " " + status : "")}>
				<select
					value={nvalue}
					onChange={e => handleSelectOption(e.target.value)}
					disabled={status == "disabled"}>
					<option value={""} key={"native_option_default"} disabled>
						{placeholder}
					</option>

					{options.map((opt, i) => {
						return (
							<option
								value={opt[customKey]}
								key={"native_option_" + "_" + opt[customKey] + "_" + i}>
								{opt[valueKey]}
							</option>
						);
					})}
				</select>
			</div>
			<style jsx>{`
				.native-select {
					height: 100%;
					width: 100%;
					border: 1px solid ${theme.colors.border};
					position: relative;
				}
				select {
					border-radius: 0px;
					border: none;
					padding: 12px 8px;
					appearance: none;
					width: 100%;
					height: 100%;
					font-size: ${theme.fontSizes.text}px;
				}
				.native-select:before {
					content: "";
					position: absolute;
					top: calc(50% - 3px);
					right: 8px;
					width: 0px;
					height: 0px;
					border: 5px solid transparent;
					border-top-color: ${theme.colors.textLighter};
				}
				.readonly {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
				}
				.disabled {
					cursor: default;
					pointer-events: none;
					touch-action: none;
					user-select: none;
					opacity: 0.5;
				}
			`}</style>
		</React.Fragment>
	);
};
