import { CheckboxSelectProps } from "../CheckboxSelect/CheckboxSelect.hook";
import { normalizeValue } from "../Selects.hook";
import { useState, useEffect } from "react";

export interface CustomSelectProps extends CheckboxSelectProps {
	firstOpen?: boolean;
}

export const useCustomSelect = (props: CustomSelectProps) => {
	const {
		firstOpen = false,
		value,
		options = [],
		customKey = "id",
		valueKey = "name",
		type = "multiple",
		placeholder = "Seleccione una opción",
		onChange,
		...selectprops
	} = props;

	const getHeaderText = (value: any) => {
		let res = "";
		if (Array.isArray(value)) {
			value.forEach((o, i) => {
				res += o[valueKey] + (i < value.length - 1 ? ", " : "");
			});
		} else if (value != null) res += value[valueKey];
		return res;
	};

	const getDefaultTitle = () => {
		const firstValueKeys = normalizeValue(value, type, customKey);
		const firstValue = options.filter((opt) => {
			if (Array.isArray(firstValueKeys)) return firstValueKeys.includes(opt[customKey]);
			else if (firstValueKeys) return firstValueKeys == opt[customKey];
			else return false;
		});
		return getHeaderText(firstValue);
	};

	const [title, setTitle] = useState(getDefaultTitle());
	const [open, setOpen] = useState(firstOpen);

	useEffect(() => setTitle(getDefaultTitle()), [value]);

	return {
		open,
		setOpen,
		title,

		placeholder,
		value,
		type,
		options,
		customKey,
		valueKey,
		onChange,
		...selectprops,
	};
};
