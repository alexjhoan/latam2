import { FC } from "react";
import theme from "../../../../../Styles/_toBeDeleted_theme";
import { WindowEventListener } from "../../../../../GlobalHooks/web/WindowEventListener.hook";
import { SmartDropdownOptionProps, useSmartDropdownOption } from "./SmartDropdownOption.hook";
import { Icon } from "../../../../Icon/web";

export const SmartDropdownOption: FC<SmartDropdownOptionProps> = props => {
	const {
		data,
		navCursorRef,
		isNavCursor,
		onSelect,
		valueKey,
		groupKey,
		boldMatch,
		match,
	} = useSmartDropdownOption(props);

	/* key press */
	const handleKeydown = e => {
		if (e.keyCode == 13 && isNavCursor) onSelect(data);
	};
	WindowEventListener("keydown", handleKeydown, false);
	/* end key press */

	/* bold match with string */
	const boldTextMatch = string => {
		if (!boldMatch) return string;

		return string.replace(
			new RegExp(match.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()]/g, ""), "gi"),
			function(a, b) {
				return "<b>" + a + "</b>";
			}
		);
	};
	/* end bold match with string */

	return (
		<>
			<div
				ref={navCursorRef}
				onClick={() => onSelect(data)}
				className={"smart-dropdown-option" + (isNavCursor ? " is-current-nav-cursor" : "")}>
				<div className="label">
					{data["icon"] && <Icon name={data["icon"]} />}
					<span
						className={"text"}
						dangerouslySetInnerHTML={{
							__html: boldTextMatch(data[valueKey]),
						}}></span>
				</div>
				<div className="group-text">{data[groupKey]}</div>
			</div>
			<style jsx>{`
				.smart-dropdown-option {
					padding: 8px;
					width: 100%;
					box-sizing: border-box;
					background: ${theme.colors.background};
					color: ${theme.colors.text};
					display: flex;
					justify-content: space-between;
				}
				.smart-dropdown-option.isSelected {
					background: ${theme.colors.primaryOpacity};
					color: ${theme.colors.textHover};
				}
				.smart-dropdown-option.is-current-nav-cursor,
				.smart-dropdown-option:hover {
					background: ${theme.colors.backgroundLight};
					color: ${theme.colors.textHover};
				}
				.label :global(i) {
					margin: 0 0.2em;
					margin-right: 0.4em;
					color: ${theme.colors.text};
				}
				.group-text {
					color: ${theme.colors.textLighter};
				}
			`}</style>
		</>
	);
};
