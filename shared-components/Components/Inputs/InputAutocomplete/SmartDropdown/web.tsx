import { useSmartDropdown, SmartDropdownProps } from "./SmartDropdown.hook";
import { SmartDropdownOption } from "./SmartDropdownOption/web";
import { FC, useRef, useEffect } from "react";
import theme from "../../../../Styles/_toBeDeleted_theme";
import { WindowEventListener } from "../../../../GlobalHooks/web/WindowEventListener.hook";

export const SmartDropdown: FC<SmartDropdownProps> = props => {
	const {
		status,
		navOptions,
		navCursor,
		increaseNavIndex,
		decreaseNavIndex,
		getNavIndexPrefix,
		customKey,
		emptyText,
		...optionprops
	} = useSmartDropdown(props);

	/* key press */
	const handleKeydown = e => {
		if (e.keyCode == 38) decreaseNavIndex();
		else if (e.keyCode == 40) increaseNavIndex();
	};
	WindowEventListener("keydown", handleKeydown, false);
	/* end key press */

	/* cursor option on smart-combo view */
	const containerRef = useRef(null);
	const navCursorRef = useRef(null);
	useEffect(() => {
		if (navCursorRef.current != null && navCursorRef.current != null) {
			containerRef.current.scrollTop =
				navCursorRef.current.offsetTop > containerRef.current.clientHeight - 30
					? navCursorRef.current.offsetTop -
					  containerRef.current.clientHeight +
					  navCursorRef.current.clientHeight
					: 0;
		}
	});
	/* end cursor option on smart-combo view */

	return (
		<>
			<div ref={containerRef} className={"smart-combo" + (status ? " " + status : "")}>
				{navOptions.length > 0 ? (
					navOptions.map(({ title, data: cOptions }, iNavGroup) => {
						const iNavPrefix = getNavIndexPrefix(iNavGroup);
						return (
							<div
								className={"smart-combo-group " + title}
								key={"smart_combo_group_" + title + "_" + iNavGroup}>
								{typeof title != "undefined" && (
									<div
										className={
											"smart-combo-title" +
											(title.length == 0 ? " separator" : "")
										}>
										{title}
									</div>
								)}

								<div className={"smart-combo-options"}>
									{cOptions.map((opt, i) => {
										const currIndexNav = iNavPrefix + i;
										return (
											<SmartDropdownOption
												key={
													"smart_option_" +
													currIndexNav +
													"_" +
													opt[customKey]
												}
												data={opt}
												navCursorRef={
													currIndexNav == navCursor ? navCursorRef : null
												}
												isNavCursor={currIndexNav == navCursor}
												customKey={customKey}
												{...optionprops}
											/>
										);
									})}
								</div>
							</div>
						);
					})
				) : (
					<div className="smart-combo-empty-text">{emptyText}</div>
				)}
			</div>
			<style jsx>{`
				.smart-combo {
					width: 100%;
					max-height: 200px;
					overflow-y: auto;
					overflow-x: hidden;
				}
				.smart-combo-title {
					background: ${theme.colors.backgroundHover};
					color: ${theme.colors.textLight};
					padding: 8px;
					font-size: 12px;
					font-weight: bold;
					text-transform: capitalize;
				}
				.smart-combo-title.separator {
					padding: 1px;
				}
				.smart-combo-empty-text {
					padding: 0px 8px;
					line-height: 45px;
				}
			`}</style>
		</>
	);
};
