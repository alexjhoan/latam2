import React, { FC } from "react";
import {
	InputAutocompleteProps,
	useInputAutocomplete,
} from "./InputAutocomplete.hook";
import {
	Text,
	Platform,
	View,
	KeyboardAvoidingView,
	TouchableWithoutFeedback,
	Keyboard,
	SafeAreaView,
	Modal,
	StyleSheet,
} from "react-native";
import { Input } from "../Input/app";
import { SmartDropdown } from "./SmartDropdown/app";
import { Button } from "../Button/app";
import { Icon } from "../../Icon/app";
import { IconNames } from "../../Icon/Icon.hook";
import theme from "../../../Styles/theme";
import { TouchableOpacity } from "react-native-gesture-handler";

export const InputAutocomplete: FC<InputAutocompleteProps> = (
	props: InputAutocompleteProps
) => {
	const {
		status,
		openOnEmpty,
		open,
		setOpen,
		clear,
		onClear,
		handleBlur,
		handleFocus,

		required,
		name,
		type,
		input,
		inputFocus,
		placeholder,
		disabled,
		onInputChange,

		...smartdropdownprops
	} = useInputAutocomplete(props);

	const showOptions = open && (input.length > 0 || openOnEmpty);
	const showCleanButton = clear && input != "";

	const inputComponentAutocomplete = (
		<View style={open ? styles.headerOpen : styles.header}>
			<View style={styles.inputContainer}>
				<Input
					onChange={onInputChange}
					defaultValue={input}
					placeholder={placeholder}
					type={type}
					required={required}
					onFocus={handleFocus}
					focused={open || inputFocus}
					// disabled: status == 'disabled'
				/>
				{showCleanButton && (
					<View style={styles.clearButton}>
						<Button
							content={<Icon name={IconNames["cancel"]} size={18} />}
							handleClick={() => onClear()}
							type="text"
						/>
					</View>
				)}
			</View>
			{open && (
				<View style={styles.closeButton}>
					<Button
						content={"cerrar"}
						handleClick={() => setOpen(false)}
						type="text"
					/>
				</View>
			)}
		</View>
	);

	return (
		<>
			{!open && (
				<TouchableOpacity onPress={() => setOpen(true)}>
					{inputComponentAutocomplete}
				</TouchableOpacity>
			)}
			<Modal visible={open}>
				<SafeAreaView>
					<KeyboardAvoidingView
						behavior={Platform.OS === "ios" ? "padding" : "height"}
						enabled={open}>
						<TouchableWithoutFeedback onPress={Keyboard.dismiss}>
							<View
								style={{
									height: open ? "100%" : "auto",
									backgroundColor: theme.colors.background,
								}}>
								{inputComponentAutocomplete}
								{showOptions && (
									<SmartDropdown match={input} {...smartdropdownprops} />
								)}
							</View>
						</TouchableWithoutFeedback>
					</KeyboardAvoidingView>
				</SafeAreaView>
			</Modal>
		</>
	);
};

const styles = StyleSheet.create({
	header: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		width: "100%",
	},
	headerOpen: {
		flexDirection: "row",
		alignItems: "center",
		justifyContent: "space-between",
		width: "100%",
		paddingHorizontal: 10,
		paddingVertical: 8,
	},
	showText: {
		color: theme.colors.text,
		fontSize: theme.fontSizes.appText,
	},
	placeholder: {
		color: theme.colors.textLight,
		fontSize: theme.fontSizes.appText,
	},
	inputContainer: { flexGrow: 2 },
	clearButton: { position: "absolute", right: 0 },
	closeButton: { flexGrow: 0 },
});
