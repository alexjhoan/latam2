import React from "react";
import cookie from "js-cookie";
import { Avatar as AvatarComponent, Typography, Menu, Dropdown, Button } from "antd";
import { useTranslation } from "react-i18next";
import { useUser } from "../User.hook";
import { UserOutlined } from "@ant-design/icons";

export const Avatar = () => {
	const { logout, user } = useUser();
	if (!user) return <></>;
	const { t } = useTranslation();

	const overlay = (
		<Menu>
			<Menu.Item
				onClick={() =>
					logout.send().then(() => {
						cookie.remove("frontend_token");
						localStorage.removeItem("user_md5");
						location.reload();
					})
				}>
				{t("Salir")}
			</Menu.Item>
		</Menu>
	);

	return (
		<Dropdown overlay={overlay} placement="bottomCenter" arrow>
			<Button type={"text"} icon={<UserOutlined />}>
				{user?.data?.me.name}
			</Button>
		</Dropdown>
	);
};
