import gql from "graphql-tag";

export const CURRENT_USER_QUERY = gql`
	query ME_HEADER {
		me {
			__typename
			firstName
			lastName
			name
			id
			history {
				property_id
			}
			email
			individual
			logo
			country_id
			property_count
			phone
			notifications(first: 5) {
				data {
					id
					created_at
					text
					url
					new_tab
					seen
					image
				}
			}
			unread_notifications
		}
	}
`;

export const LOGOUT_MUTATION = gql`
	mutation LOGOUT {
		logout {
			status
			message
			__typename
		}
	}
`;

export const LOGIN_MUTATION = gql`
	mutation Login($email: String!, $pass: String!) {
		login(input: { username: $email, password: $pass }) {
			access_token
			user_md5
		}
	}
`;

export const REGISTER_MUTATION = gql`
	mutation Register($email: String!, $pass: String, $name: String) {
		registerIndividual(input: { email: $email, password: $pass, name: $name }) {
			access_token
			user_md5
		}
	}
`;

export const SOCIAL_LOGIN_MUTATION = gql`
	mutation socialLogin($token: String!, $provider: String!, $email: String) {
		socialLogin(token: $token, provider: $provider, email: $email) {
			access_token
			user_md5
		}
	}
`;

export const USER_EXISTS_QUERY = gql`
	query userExists($email: String!) {
		userExists(email: $email) {
			id
		}
	}
`;

export const FORGOT_PASSWORD_MUTATION = gql`
	mutation forgotPassword($email: String!) {
		forgotPassword(input: { email: $email })
	}
`;

export const GOOGLE_ONE_TAP_MUTATION = gql`
	mutation googleOneTapLogin($token: String!) {
		googleOneTapLogin(token: $token) {
			access_token
			refresh_token
			expires_in
			user_md5
		}
	}
`;
