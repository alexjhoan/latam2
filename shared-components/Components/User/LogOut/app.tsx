import React from "react";
import { useUser } from "../User.hook"
import { LocalStorage } from "../../../../src/LocalStorage";
import { Button } from "../../Inputs/Button/app";
import { DefaultLocalState } from "../../../ApolloResolvers";



const Logout = () => {
    const { logout } = useUser()
    const logoutCleanUp = (client: any) => {
        client.writeData({ data: { ...DefaultLocalState } });
        LocalStorage.remove('authToken')
    }

    return (
        <Button content="Log out" type="primary" handleClick={() => logout(logoutCleanUp)} />
    )
}


export { Logout }