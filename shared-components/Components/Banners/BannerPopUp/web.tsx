import "./styles.less";

import React, { useEffect, useState } from "react";

import { CloseCircleFilled } from "@ant-design/icons";
import { ImagenOptimizada } from "../../Image/web";
import { Modal } from "antd";
import { useBannerPopUp } from "./BannerPopUp.hook";
import { useTheme } from "../../../Styles/ThemeHook";

const LOCAL_STORAGE_KEY = "popup_dinamico";

export const BannerPopUp = () => {
	const { theme } = useTheme();
	const { data, error } = useBannerPopUp();
	const [isOpen, setOpen] = useState(false);

	useEffect(() => {
		const lastTime = localStorage.getItem(LOCAL_STORAGE_KEY);
		if (
			data &&
			(data.image != null || data.video != null) &&
			(!lastTime || Date.now() - parseInt(lastTime) > 1000 * 60 * 60 * 24)
		) {
			setOpen(true);
			let date = Date.now();
			localStorage.setItem(LOCAL_STORAGE_KEY, date.toString());
		} else if (error) {
			setOpen(false);
		}
	}, [data, error]);

	if (!isOpen) return null;

	if (data.video != null) {
		data["video_ext"] = data.video
			.split(/\#|\?/)[0]
			.split(".")
			.pop()
			.trim();
	}

	const onClose = () => setOpen(false);

	return (
		<React.Fragment>
			<Modal
				width="740px"
				className="modal-popup"
				visible={isOpen}
				onCancel={onClose}
				footer={null}
				centered
				maskClosable
				keyboard
				closeIcon={<CloseCircleFilled />}>
				<div className="content">
					<a
						target="_blank"
						href={data.url}
						title={data.title}
						className="link"
						onClick={() => onClose()}>
						{data.video != null ? (
							<video autoPlay loop muted poster={data.video_thumbnail}>
								<source src={data.video} type={"video/" + data.video_ext} />
							</video>
						) : (
							<picture>
								{data.mobile_image != null ? (
									<source media="(max-width: 980px)" srcSet={data.mobile_image} />
								) : null}
								<ImagenOptimizada src={data.image} alt={data.title} />
							</picture>
						)}
					</a>
				</div>
			</Modal>

			<style jsx global>{`
				.modal-popup .ant-modal-content {
					border-radius: ${theme.spacing.smSpacing}px;
					background: transparent;
				}

				.modal-popup .ant-modal-content .ant-modal-close {
					color: ${theme.colors.borderColor};
				}

				.modal-popup .ant-modal-content .ant-modal-close svg:hover {
					color: ${theme.colors.textColor};
				}

				.modal-popup .content video,
				.modal-popup .content img {
					border-radius: ${theme.spacing.smSpacing}px;
				}
			`}</style>
		</React.Fragment>
	);
};
