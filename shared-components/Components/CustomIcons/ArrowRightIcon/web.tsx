import Icon from "@ant-design/icons";

const ArrowRightSvg = () => (
	<svg width="10" height="10" viewBox="0 0 6 10" fill="none" xmlns="http://www.w3.org/2000/svg">
		<path
			d="M3.92118 5.00503L0 1.05884L1.02487 0L6 5.00671L1.02432 10L0.000548345 8.93948L3.92118 5.00503Z"
			fill="#85909A"
		/>
	</svg>
);

export const ArrowRightIcon = props => <Icon component={ArrowRightSvg} {...props} />;
