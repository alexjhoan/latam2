import gql from "graphql-tag";
import {ApolloError, useQuery} from "@apollo/client";
import {BigData} from "../model";
import { useState } from "react";
import { parseMoney } from "../../../Utils/Functions";

const QUERY_BIGDATA = gql`
    query BigData($params: SearchParamsInput!, $quantity: Int!) {
        bigData(params: $params, getFirst: $quantity) {
            id
            count_results
            created_at
            avg_price_m2
            avg_price
            avg_time_in_market
            currency {
                name
            }
        }
    }
`;

export interface BigDataHookResponse {
    bigData: {
        loading: boolean
        error: ApolloError
        data: BigData | null
    }
    bigDataMonthly: {
        loading: boolean
        error: ApolloError
        data: BigData[] | null,
        currency: any
    }
    setQuantity: Function,
    quantity: number
}

export const useBigDataSearchResult = ({searchFilters}) : BigDataHookResponse => {
    const [quantity, setQuantity] = useState(1);
    const {loading:loadingSingle, error:errorSingle, data:dataSingle} = useQuery(QUERY_BIGDATA, {
        variables: {
            params: {...searchFilters,page:1},
            quantity: 1
        },
        skip: !searchFilters
    });
    const {loading:loadingMonthly, error:errorMonthly, data:dataMonthly} = useQuery(QUERY_BIGDATA, {
        variables: {
            params: {...searchFilters,page:1},
            quantity
        },
        skip: !searchFilters || quantity==1
    });

    let bigDataSingle = null;
    if(dataSingle && dataSingle.bigData && dataSingle.bigData.length > 0) {
        bigDataSingle = dataSingle.bigData[0];
        const { avg_price, avg_price_m2, currency  } = bigDataSingle

        bigDataSingle = {
            ...bigDataSingle,
            avg_price_text: `${currency.name} ${parseMoney(avg_price,0,2)}`,
            avg_price_m2_text: `${currency.name} ${parseMoney(avg_price_m2,0, 1)}`
        }
    }

    let bigDataMonthly = [];
    let currency = null;
    if(dataMonthly && dataMonthly.bigData && dataMonthly.bigData.length > 0) {
        currency = dataMonthly.bigData[0].currency;
        bigDataMonthly = dataMonthly.bigData.map(data => {
            return {
                ...data,
                avg_price_text: `${data.currency.name} ${parseMoney(data.avg_price,0,2)}`,
                avg_price_m2_text: `${data.currency.name} ${parseMoney(data.avg_price_m2,0, 1)}`
            }
        }).reverse()
    }

    return {
        bigData: {
            loading: loadingSingle,
            error: errorSingle,
            data: bigDataSingle,
        },
        bigDataMonthly: {
            loading: loadingMonthly,
            error: errorMonthly,
            data: bigDataMonthly,
            currency
        },
        quantity,
        setQuantity
    }
};
