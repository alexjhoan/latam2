import React, { useContext, useEffect, useState } from "react";

import { ConfigStateContext } from "../../../Contexts/Configurations/context";
import Head from "next/head";
import getConfig from "next/config";
import { useImage } from "../../Image/Image.hook";
import { useSEO } from "../hook";
import { useTheme } from "../../../Styles/ThemeHook";

const { NODE_ENV } = getConfig().publicRuntimeConfig;

export const MetaTags = () => {
	const { seoMetaTags, error } = useSEO();
	const { getImagenOptimizada } = useImage();
	const { pinterest_id } = useContext(ConfigStateContext);

	const { theme } = useTheme();

	if (error || !seoMetaTags) return null;

	// const [description, setDescription] = useState("");

	// useEffect(() => {
	// 	let spaceDesc = seoMetaTags?.description.split(" ");
	// 	if (spaceDesc.length > 35) {
	// 		let newDescription = spaceDesc.slice(0, 35).join(" ") + "...";
	// 		return setDescription(newDescription);
	// 	} else {
	// 		let newDescription = spaceDesc.join(" ");
	// 		return setDescription(newDescription);
	// 	}
	// });

	const og_image = getImagenOptimizada({
		src: seoMetaTags.og_image,
		width: 1200,
		height: 400,
	});

	return (
		<>
			<Head>
				<link rel="icon" href={theme.images.favicon} type="image/x-icon" />
				<meta httpEquiv="Content-Type" content="text/html; charset=utf-8" />
				<meta name="author" content="InfoCasas" />
				<meta
					name="viewport"
					content="width=device-width, initial-scale=1.0, maximum-scale=1.0,user-scalable=0"
				/>
				<title>{seoMetaTags.title || "InfoCasas"}</title>
				<link rel="canonical" href={seoMetaTags.canonical} />
				<meta name={seoMetaTags.description} />
				<meta property="og:url" content={seoMetaTags.url} />
				<meta property="og:type" content="website" />
				<meta property="og:title" content={seoMetaTags.title} />
				<meta property="og:description" content={seoMetaTags.description} />
				<meta property="og:image" content={og_image} />
				<meta property="og:site_name" content={seoMetaTags.site_name} />
				<meta name="keywords" content="Inmuebles, casas, apartamentos, alquiler, venta" />
				<meta name="p:domain_verify" content={pinterest_id} />
				{NODE_ENV != "production" && <meta name="robots" content="noindex, nofollow" />}
			</Head>
		</>
	);
};
