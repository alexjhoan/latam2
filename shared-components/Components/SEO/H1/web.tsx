import { Skeleton, Typography } from "antd";

import { useSEO } from "../hook";

const { Title } = Typography;

export const H1 = () => {
	const { seoMetaTags, error, loading } = useSEO();

	if (loading) return <Skeleton title paragraph={false} active />;
	if (error || !seoMetaTags) return null;

	return <Title>{seoMetaTags.h1}</Title>;
};
