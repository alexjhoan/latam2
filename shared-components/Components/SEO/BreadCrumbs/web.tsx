import { Breadcrumb, Skeleton, Typography } from "antd";

import { ArrowRightIcon } from "../../CustomIcons/web";
import Head from "next/head";
import React from "react";
import { formatMoney } from "../../../Utils/Functions";
import { useSEO } from "../hook";
import { useTheme } from "../../../Styles/ThemeHook";

const { Text, Link } = Typography;

export const BreadCrumbs = () => {
	const { seoMetaTags, error, loading } = useSEO();
	const { theme } = useTheme();

	if (loading)
		return (
			<Skeleton
				title={false}
				paragraph={{ rows: 1, width: 500, style: { margin: 0 } }}
				active
			/>
		);
	if (error || !seoMetaTags) return null;

	const jsonData = {
		"@context": "https://schema.org",
		"@type": "BreadcrumbList",
		itemListElement: seoMetaTags.breadcrumbs?.map((item, i) => ({
			"@type": "ListItem",
			position: i + 1,
			name: item.text,
			item: "https://www.infocasas.com.uy" + item.url,
		})),
	};

	return (
		<React.Fragment>
			<Head>
				<script
					type="application/ld+json"
					dangerouslySetInnerHTML={{ __html: JSON.stringify(jsonData) }}></script>
			</Head>
			<Breadcrumb
				className={"custom-page-breadcrumb"}
				routes={seoMetaTags.breadcrumbs.map((b, i) => ({
					...b,
					breadcrumbName: b.text,
					path: "breadcrumb_" + b.text + "_" + i,
				}))}
				itemRender={breadcrumbItemRender}
				separator={<ArrowRightIcon />}
			/>
			<style jsx global>{`
				.custom-page-breadcrumb.ant-breadcrumb {
					display: inline;
				}
				.custom-page-breadcrumb.ant-breadcrumb * {
					font-size: ${theme.fontSizes.smFontSize};
					display: inline-block;
					max-width: 100%;
				}
				.custom-page-breadcrumb.ant-breadcrumb a {
					text-transform: capitalize;
				}
				.custom-page-breadcrumb.ant-breadcrumb *:not(a) {
					color: ${theme.colors.textSecondaryColor};
				}
				.custom-page-breadcrumb > span a {
					color: ${theme.colors.textSecondaryColor};
				}
				.pd-navigation .custom-page-breadcrumb.ant-breadcrumb * {
					color: ${theme.colors.textSecondaryColor};
					display: inline;
				}
			`}</style>
		</React.Fragment>
	);
};

function breadcrumbItemRender(route, params, routes, paths) {
	let cadena = route.text;
	if (cadena.indexOf("página") !== -1) {
		const page = cadena.substring(7, cadena.length);
		cadena = cadena.replace(page, formatMoney(page));
	} else null;

	const first = routes.findIndex(r => r.path == route.path) === 0;
	const item = (
		<Link href={route.url} ellipsis>
			{cadena}
		</Link>
	);

	return (
		<React.Fragment>
			{first ? (
				<Text
					style={{
						whiteSpace: "nowrap",
					}}>
					Estas en: {item}
				</Text>
			) : (
				item
			)}
		</React.Fragment>
	);
}
