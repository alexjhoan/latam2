import "./styles.less";

import { Col, Skeleton } from "antd";
import { ProjectInfoItem, useProjectInfo } from "./hook";

import { List } from "antd";

export const ProjectInfo = ({ id }: { id: string }) => {
	const { loading, data, error } = useProjectInfo(id);

	if (loading) {
		return <Skeleton active />;
	}

	if (error || !data.projectInfoList.length) {
		return null;
	}

	const ProjectInfoItem = ({ title, value }: ProjectInfoItem) => (
		<List.Item>
			<Col span={12}>
				<b>{title}</b>
			</Col>
			<Col span={12}>{value}</Col>
		</List.Item>
	);

	return (
		<List
			size="small"
			bordered
			className="project-info"
			dataSource={data.projectInfoList}
			renderItem={ProjectInfoItem}
		/>
	);
};
