import {
	FragmentDefiner,
	useReadFragment,
} from '../../../GlobalHooks/useReadFragment';
import { useEffect, useState } from 'react';
import { useTranslation } from 'react-i18next';
import { GraphQLError } from 'graphql';

export const FRAGMENT_PROJECT_INFO = new FragmentDefiner(
	'Property',
	`
    project {
        architect
        percentage_sold
        percentage_finished
		ocupation
		owner {
			name
		}
    }
`
);

const pojectInfoTitles = Object.freeze({
	architect: 'Arquitecto',
	percentage_sold: 'Porcentaje vendido',
	percentage_finished: 'Porcentaje terminado',
	ocupation: 'Ocupación',
	owner: 'Desarrolla',
});

type ProjectInfo = {
	architect: string;
	percentage_sold: number | null;
	percentage_finished: number | null;
	ocupation: Date | null;
	owner: Object | null;
};

export type ProjectInfoItem = {
	title: string;
	value: string;
};

export interface useProjectInfoResponse {
	loading: boolean;
	data: {
		projectInfo: ProjectInfo;
		projectInfoList: ProjectInfoItem[];
	};
	error: GraphQLError;
}

export const useProjectInfo = (id: string): useProjectInfoResponse => {
	const { loading, data, error } = useReadFragment(FRAGMENT_PROJECT_INFO, id);
	const [projectInfoList, setProjectInfoList] = useState<ProjectInfoItem[]>([]);
	const [projectInfo, setProjectInfo] = useState<ProjectInfo>();
	const { t } = useTranslation();

	const formatData = (key: string, value: any): string => {
		switch (key) {
			case 'percentage_sold':
				return `${value}%`;
			case 'percentage_finished':
				return `${value}%`;
			case 'ocupation':
				const date = new Date(value);
				return `${t('MONTH_' + date.getMonth())} ${date.getUTCFullYear()}`;
			case 'owner':
				return value?.name ?? '';
			default:
				return value;
		}
	};

	const makeProjectInfoList = (info: ProjectInfo): ProjectInfoItem[] => {
		return Object.entries(info)
			.filter(([_, value]) => value) // Solo info con valor.
			.map(([key, value]) => ({
				title: t(pojectInfoTitles[key]), // Traducción
				value: formatData(key, value),
			}));
	};

	useEffect(() => {
		if (!data?.project.length) {
			return;
		}

		const info = { ...data.project[0] };
		delete info['__typename'];
		setProjectInfo(info);
		setProjectInfoList(makeProjectInfoList(info));
	}, [data]);

	return {
		loading,
		data: {
			projectInfo, // Info original
			projectInfoList,
		},
		error,
	};
};
