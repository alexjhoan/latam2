import "./styles.less";

import { Col, List, Radio, Row, Skeleton, Typography } from "antd";
import { useEffect, useState } from "react";

import { PageLink } from "../../Link/web";
import { makePriceText } from "../../../Utils/Functions";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { useUnits } from "./hook";

export const Units = ({ id }) => {
	const { filters, units, sendLead, loading } = useUnits({ id });
	const [filterBy, setFilterBy] = useState("all");
	const [data, setData] = useState([]);
	const [asked, setAsked] = useState([]);
	const [relatedModal, setRelatedModal] = useState(false);
	const { t } = useTranslation();
	const { theme } = useTheme();

	useEffect(() => {
		if (filterBy === "all") {
			setData(units);
		} else {
			setData(filters[filterBy].units);
		}
	}, [units, filterBy]);

	const radioChange = e => {
		setFilterBy(e.target.value);
	};

	const askDisponibility = unitID => {
		sendLead(unitID).then(data => {
			if (data) {
				setAsked([...asked, unitID]);
				setRelatedModal(true);
			}
		});
	};

	if (loading) return <Skeleton active />;

	const UnitItem = ({ id, code, price, bedrooms, bathrooms, m2, link }) => (
		<List.Item>
			<Col md={5} xs={5} className="project-units-list-ref">
				<PageLink pathname="/singleProps" as={link}>
					#{code}
				</PageLink>
			</Col>
			<Col md={8} xs={5} className="project-units-items">
				<Row>
					<Col xs={24} md={8}>
						{bedrooms === 0 ? t("Mono") : `${bedrooms}${t("dorm")}`}
					</Col>
					<Col xs={24} md={8}>
						{bathrooms}
						{t("bañ")}
					</Col>
					<Col xs={24} md={8}>
						{m2} m2
					</Col>
				</Row>
			</Col>
			<Col md={5} xs={6}>
				{makePriceText(price)}
			</Col>
			<Col md={6} xs={8}>
				{asked.some(elem => elem == id) ? (
					<Typography.Text disabled>{t("¡Preguntado!")}</Typography.Text>
				) : (
					<Typography.Link onClick={() => askDisponibility(id)}>
						{t("Averiguar Disponibilidad")}
					</Typography.Link>
				)}
			</Col>
		</List.Item>
	);

	return (
		<div className="project-units-section">
			<Radio.Group
				className="project-units-filters"
				onChange={radioChange}
				value={filterBy}
				style={{ marginBottom: 15 }}>
				<Radio.Button value="all">
					{t("Todos")} ({units.length})
				</Radio.Button>
				{filters.length > 1
					? filters.map((filter, i) => (
							<Radio.Button key={i} value={i}>
								{filter.beds == 0
									? `${t("Mono")} (${filter.units.length})`
									: `${filter.beds} ${t("dorm")} (${filter.units.length})`}{" "}
							</Radio.Button>
					  ))
					: null}
			</Radio.Group>
			<List
				size="small"
				bordered
				className="project-units-list"
				dataSource={data}
				renderItem={prop => <UnitItem {...prop} />}
			/>
			<style jsx global>{`
				.project-units-filters
					.ant-radio-button-wrapper-checked:not(.ant-radio-button-wrapper-disabled) {
					background: ${theme.colors.secondaryColor} !important;
					color: white !important;
					border-color: transparent !important;
				}
				.project-units-filters .ant-radio-button-wrapper {
					padding: 0 ${theme.spacing.smSpacing}px;
					font-size: ${theme.fontSizes.smFontSize};
				}
			`}</style>
		</div>
	);
};
