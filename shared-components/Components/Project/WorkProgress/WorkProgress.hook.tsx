import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { useEffect, useState } from "react";
import { GraphQLError } from "graphql";
import { Image } from "../../Property/PropertyInterfaces";

export const FRAGMENT_WORK_PROGRESS = new FragmentDefiner(
	"Property",
	`
    id
    project {
		images {
            image
            tag
        }
    }
`
);

interface useWorkProgressResponse {
	error: GraphQLError;
	loading: boolean;
	images: Image[];
}

export const useWorkProgress = (id: string): useWorkProgressResponse => {
	const { loading, data, error } = useReadFragment(FRAGMENT_WORK_PROGRESS, id);
	const [images, setImages] = useState<Image[]>([]);

	useEffect(() => {
		if (data?.project.length) {
			const workProgressImages = data.project[0].images.filter(
				image => image.tag == "construction_advances"
			);
			setImages(workProgressImages);
		}
	}, [data]);

	return {
		loading,
		images,
		error,
	};
};
