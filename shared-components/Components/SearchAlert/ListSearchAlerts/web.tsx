import React from "react";
import { useListSearchAlerts } from "./hook";
import { useUpdateSearchAlert } from "../UpdateSearchAlert/hook";
import { Button } from "../../Inputs/web";

export const ListSearchAlerts = () => {
	const { data, loading, error } = useListSearchAlerts();
	const { updateSearchAlert } = useUpdateSearchAlert();

	if (loading) return <>Loading Alerts...</>;
	if (error) return <>Error Loading Alerts...</>;

	return (
		<>
			{[...data].map(searchAlert => (
				<div>
					<p>{searchAlert.title}</p>
					<p>Frecuencia: {searchAlert.frequency}</p>
					<Button
						content={"Status:" + searchAlert.status}
						handleClick={() =>
							updateSearchAlert({
								id: searchAlert.id,
								status: !searchAlert.status,
							})
						}
					/>
				</div>
			))}
		</>
	);
};
