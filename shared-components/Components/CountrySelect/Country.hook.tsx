import React, { useState, createContext } from 'react';
import gql from 'graphql-tag';
import { useQuery, useMutation } from '@apollo/client';
import { MUTATION_LOCAL_STORE } from '../../_toBeDeleted_ApolloResolvers';
import CountryResolver, { QUERY_COUNTRY, TYPENAME_COUNTRY } from '../../_toBeDeleted_ApolloResolvers/Country';


const QUERY_COUNTRIES_LIST = gql`{
    countries{
        id
        name
        order
        data{
            country_id
            country_name
            country_code
            main_domain
            country_flag
            __typename
        }
    }
  }
`;


const useCountrySelection = () => {
  const { data: dataCountry } = useQuery(QUERY_COUNTRY);
  const country = dataCountry ? dataCountry[TYPENAME_COUNTRY] : null;

  const [selected, setCountry] = useState(country);

  const { data, loading, error } = useQuery(QUERY_COUNTRIES_LIST);
  const [MUTATOR_LOCAL_STORE] = useMutation(MUTATION_LOCAL_STORE);

  const save = (c: any) => {
    return MUTATOR_LOCAL_STORE({
      variables: {
        new_state: { ...c },
        query: QUERY_COUNTRY,
      },
    });
  };

  const remove = () => save({ country_id: null });

  return {
    country: country,
    countryList: { data: data, loading: loading, error: error },
    selected: selected,
    select: setCountry,
    save: save,
    remove: remove,
  };
};


export { useCountrySelection };
