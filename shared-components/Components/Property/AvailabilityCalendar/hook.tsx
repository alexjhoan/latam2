import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import dayjs, { Dayjs } from "dayjs";
import { useEffect, useState } from "react";

import isSameOrBefore from "dayjs/plugin/isSameOrBefore";

export const FRAGMENT_OCCUPANCIES = new FragmentDefiner(
	"Property",
	`
  occupancies {
    date_begin
    date_end
  }
`
);

interface AvailabilityCalendarHook {
	firstMonth: Dayjs;
	secondMonth: Dayjs;
	nextMonth: Function;
	prevMonth: Function | Boolean;
	occupancies: String[];
	loading: Boolean;
}

export const useAvailabilityCalendar = ({ id }): AvailabilityCalendarHook => {
	dayjs.locale("es");
	dayjs.extend(isSameOrBefore);
	const { loading, data, error } = useReadFragment(FRAGMENT_OCCUPANCIES, id);
	const [firstMonth, setFirstMonth] = useState(dayjs());
	const [secondMonth, setSecondMonth] = useState(dayjs().add(1, "month"));
	const [occupancies, setOccupancies] = useState<String[]>([]);

	const nextMonth = () => {
		setFirstMonth(firstMonth.add(1, "month"));
		setSecondMonth(secondMonth.add(1, "month"));
	};

	const prevMonth = () => {
		setFirstMonth(firstMonth.subtract(1, "month"));
		setSecondMonth(secondMonth.subtract(1, "month"));
	};

	useEffect(() => {
		if (data) {
			let totalOccupancies = [];
			data.occupancies.forEach(occupancie => {
				let begin = dayjs(occupancie.date_begin);
				let end = dayjs(occupancie.date_end);
				while (begin.isSameOrBefore(end, "day")) {
					totalOccupancies.push(begin.format("YYYY-MM-DD"));
					begin = begin.add(1, "day");
				}
			});
			setOccupancies(totalOccupancies);
		}
	}, [data]);

	return {
		loading,
		firstMonth,
		secondMonth,
		nextMonth,
		prevMonth: firstMonth.isAfter(dayjs(), "month") ? prevMonth : false,
		occupancies,
	};
};
