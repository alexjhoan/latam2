import {FragmentDefiner, useReadFragment} from "../../../../GlobalHooks/useReadFragment";
import {Subsidiary} from "./model";
import {gql} from "@apollo/client/core";
import {useMutation} from "@apollo/client";

export const FRAGMENT_SUBSIDIARY_MASKED = new FragmentDefiner("Subsidiary",`
    id
    name
    masked_phone
    address
    office_hours
`);

const REQUEST_PHONE_MUTATION = gql`
    mutation request_phone_mutation($subsidiary_id: Int!, $property_id: Int!) {
        requestPhone(subsidiary_id: $subsidiary_id, property_id: $property_id)
    }
`

interface useSubsidiaryResponse {
    subsidiary: Subsidiary,
    loading: boolean,
    phone: string,
    viewPhone: CallableFunction
}
export const useSubsidiary = ({subsidiary_id}) : useSubsidiaryResponse => {
    const {data} = useReadFragment(FRAGMENT_SUBSIDIARY_MASKED, subsidiary_id)

    const [mutation, {loading, data: mutation_response}] = useMutation(REQUEST_PHONE_MUTATION, {
        onError: () => {}
    });

    const viewPhone = (property_id) => {
        return mutation_response ?? mutation({
            variables: {
                subsidiary_id,
                property_id
            }
        })
    }

    return {
        subsidiary: data,
        loading,
        phone: mutation_response?.requestPhone,
        viewPhone
    }
}