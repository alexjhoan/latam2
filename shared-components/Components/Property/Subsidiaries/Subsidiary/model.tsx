export interface Subsidiary {
    id: number
    name: string
    masked_phone: string
    address: string
    office_hours: string
}