import { useMarkSeen, useSeenStatus } from "./SeenStatus.hook";
import React from "react";
import { Button, Popover } from "antd";
import { useTheme } from "../../../Styles/ThemeHook";
import { PlusOutlined, EyeFilled } from "@ant-design/icons";
import { useTranslation } from "react-i18next";

export function SeenIcon({ id }) {
	const { result, loading, error } = useSeenStatus({ id });
	const { markSeen, loading: changeLoad } = useMarkSeen();
	const { theme } = useTheme();
	const { t } = useTranslation();

	return (
		<>
			<Popover
				overlayClassName={"seen-button-popover"}
				content={result ? t("Visto") : t("Marcar como Visto")}
				placement={"right"}>
				<Button
					className="seen-button"
					onClick={() => markSeen(id)}
					shape={"circle"}
					loading={changeLoad || loading}
					icon={result ? <EyeFilled /> : <PlusOutlined />}
				/>
			</Popover>
			<style jsx>{`
				:global(.seen-button) {
					background: ${theme.colors.backgroundModalColor};
					color: ${theme.colors.backgroundColor};
					border: none;
					border-width: 0px;
					height: 26px;
					width: 26px;
					min-width: 26px;
					line-height: 30px;
					padding: 0;
				}
				:global(.seen-button:hover),
				:global(.seen-button:focus),
				:global(.seen-button:active) {
					background: ${theme.colors.textInverseColor};
					color: ${theme.colors.backgroundColor};
				}
				:global(.seen-button-popover .ant-popover-inner-content) {
					padding: ${theme.spacing.xsSpacing}px ${theme.spacing.smSpacing}px;
					font-size: ${theme.fontSizes.xsFontSize};
					line-height: ${theme.fontSizes.baseFontSize};
				}
				:global(.property-card .seen-button) {
					left: ${theme.spacing.smSpacing}px;
					bottom: ${theme.spacing.smSpacing}px;
					position: absolute;
					z-index: 15;
				}
			`}</style>
		</>
	);
}
