export type PropComponentMode = 'property' | 'project' | 'auto';
export type Image = {
	image: string;
	tag: string;
};
