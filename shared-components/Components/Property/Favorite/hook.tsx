import { ApolloError, useMutation, useQuery } from "@apollo/client";
import { gql } from "@apollo/client";
import { useQuerySkipAuth } from "../../../GlobalHooks/useQuerySkipAuth";

const FAVORITE_QUERY = gql`
	query favorite {
		me {
			id
			... on Individual {
				favorites {
					id
					property_id
				}
			}
		}
	}
`;

const FAVORITE_MUTATION = gql`
	mutation favorite($property_id: ID!) {
		toggleFavorite(property_id: $property_id) {
			id
			... on Individual {
				favorites {
					id
					property_id
				}
			}
		}
	}
`;

export const useFavorite = ({ id }) => {
	const { data, loading, error } = useQuerySkipAuth(FAVORITE_QUERY, {
		fetchPolicy: "cache-first",
	});

	const isFavorite =
		error || loading || !data?.me.favorites
			? false
			: data.me.favorites.some(e => e.property_id == id);

	const [mutation, { data: mutationData, loading: mutationLoading }] = useMutation(
		FAVORITE_MUTATION,
		{
			variables: {
				property_id: id,
			},
			onError: error => {},
		}
	);

	const toggleFavorite = () => {
		return mutation();
	};

	return {
		loading,
		mutationLoading,
		isFavorite,
		error,
		toggleFavorite,
		isFirstFavoriteJustAdded:
			mutationData &&
			!!mutationData.toggleFavorite &&
			mutationData.toggleFavorite.favorites.length === 1,
	};
};
