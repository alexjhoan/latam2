import "./styles.less";

import { Button, Modal, Typography } from "antd";
import React, { useEffect } from "react";

import { ButtonType } from "antd/lib/button";
import { HeartIcon } from "../../CustomIcons/web";
import { useFavorite } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Text } = Typography;

export function Favorite({
	id,
	type = "default",
	fillOpacity = 0,
}: {
	id: any;
	type?: ButtonType;
	fillOpacity?: Number;
}) {
	const {
		isFavorite,
		toggleFavorite,
		isFirstFavoriteJustAdded,
		mutationLoading,
		loading,
	} = useFavorite({
		id,
	});
	const { t } = useTranslation();
	const { theme } = useTheme();

	useEffect(() => {
		if (isFavorite && isFirstFavoriteJustAdded) {
			Modal.success({
				maskClosable: true,
				title: t("Propiedad agregada a favoritos"),
				content: t(
					"Te avisaremos automaticamente cuando cambie de precio o se actualice su estado."
				),
			});
		}
	}, [isFirstFavoriteJustAdded]);

	return (
		<>
			<Button
				disabled={loading}
				className={`favorite-button secondary ${isFavorite ? "isFavorite" : ""}`}
				onClick={e => toggleFavorite()}
				type={type}
				loading={mutationLoading}
				icon={
					<HeartIcon className={"favorite-icon"} fill={isFavorite ? 1 : fillOpacity} />
				}>
				{type !== "text" ? (isFavorite ? t("Guardado") : t("Guardar")) : null}
			</Button>
			<style jsx global>{`
				.property-card .favorite-button .favorite-icon {
					color: ${theme.colors.backgroundColor};
				}
				.property-detail .favorite-button.isFavorite .favorite-icon,
				.pd-property-modal .favorite-button.isFavorite .favorite-icon {
					color: ${theme.colors.favoriteColor};
				}
				.property-detail .favorite-button.isFavorite .favorite-icon path,
				.pd-property-modal .favorite-button.isFavorite .favorite-icon path {
					fill: ${theme.colors.favoriteColor};
				}
				.property-card .favorite-button.isFavorite .favorite-icon {
					color: ${theme.colors.favoriteColor};
				}
				.property-card .favorite-button.isFavorite .favorite-icon path {
					fill: ${theme.colors.favoriteColor};
				}
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.property-detail .favorite-button .favorite-icon {
						color: ${theme.colors.backgroundColor};
					}
					.property-detail .favorite-button.isFavorite .favorite-icon {
						color: ${theme.colors.favoriteColor};
					}

					.property-detail .favorite-button:hover svg {
						color: ${theme.colors.favoriteColor};
					}
				}
			`}</style>
		</>
	);
}
