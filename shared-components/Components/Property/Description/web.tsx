import { DescriptionProps, useDescription } from "./Description.hook";
import React, { FC } from "react";
import { Skeleton, Typography } from "antd";

import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export const Description: FC<DescriptionProps> = ({
	id,
	limitLength = 3,
	hideInformation = false,
	expandable = false,
	keepLineBreaks = false,
	mode = "auto",
	showTitle = false,
}) => {
	const { t } = useTranslation();
	const { theme } = useTheme();
	const { description, loading, error } = useDescription(id, mode, keepLineBreaks);

	if (error) return null;

	const ellipsis = hideInformation
		? { rows: limitLength, expandable: expandable, symbol: t("Ver Más") }
		: false;

	return (
		<>
			{description && (
				<>
					{showTitle && <Typography.Title level={4}>{t("Descripción")}</Typography.Title>}
					{loading ? (
						<Skeleton active />
					) : (
						<Typography.Paragraph
							className={"property-description"}
							ellipsis={ellipsis}>
							<span dangerouslySetInnerHTML={{ __html: description }} />
						</Typography.Paragraph>
					)}
					<style jsx global>{`
						// NO BORRAR, ESTAMOS PROBANDO 
						// .property-card .property-description {
						// 	font-size: ${theme.fontSizes.smFontSize};
						// 	margin-bottom: ${theme.spacing.xsSpacing}px;
						// }
						.property-card .property-description {
							font-size: ${theme.fontSizes.baseFontSize};
							line-height: ${theme.fontSizes.baseLineHeight};
							margin: ${theme.spacing.smSpacing}px 0px;
						}
					`}</style>
				</>
			)}
		</>
	);
};
