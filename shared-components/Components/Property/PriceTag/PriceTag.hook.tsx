import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { makePriceText, shortNumber, isTemporal, formatMoney } from "../../../Utils/Functions";
import { useTranslation } from "react-i18next";
import { PropComponentMode } from "../PropertyInterfaces";
import { useState, useEffect } from "react";

export const FRAGMENT_PRICETAG = new FragmentDefiner(
	"Property",
	`
	id
    price_variation {
      	difference
      	percentage
      	currency {
        	name
      	}
      	date
      	amount
    }
    price {
		currency {
			name
		}
		amount
		hidePrice
    }
    commonExpenses{
		amount
		currency {
			name
		}
		hidePrice
    }
    project {
        id
    }
    operation_type {
		id
        name
	}
	temporal_price
    temporal_currency{name}
`
);

type usePriceTagProps = {
	id: string;
	threeDigitsFormat?: boolean;
	showFromText?: boolean;
	mode: PropComponentMode;
	shortExpensesText: boolean;
};

export function usePriceTag({
	id,
	threeDigitsFormat = false,
	showFromText = true,
	shortExpensesText = true,
	mode = "auto",
}: usePriceTagProps) {
	const { loading, data, error } = useReadFragment(FRAGMENT_PRICETAG, id);
	const { t } = useTranslation();
	const [price, setPrice] = useState(null);
	const [priceText, setPriceText] = useState<any>(t("Consultar"));
	const [priceVariation, setPriceVariation] = useState(null);
	const [commonExpenses, setCommonExpenses] = useState({ hide: true, text: '' });
	const [operationType, setOperationType] = useState("");

	useEffect(() => {
		if (data) {
			let isTemp: boolean = isTemporal(data.operation_type?.id);
			setPrice(data.price);
			setOperationType(data.operation_type?.name);

			const isProject = data.project?.length > 0;
			const forceProperty = mode === "property";

			if (!data.price.hidePrice && !isTemp) {
				const shortPrice = `${data.price.currency.name}${shortNumber(data.price.amount)}`;
				const largePrice = makePriceText(data.price);

				if (isProject && showFromText && !forceProperty) {
					const projectPrice = threeDigitsFormat ? shortPrice : largePrice;
					setPriceText(`${threeDigitsFormat ? "+" : t("Desde")} ${projectPrice}`);
				} else {
					setPriceText(threeDigitsFormat ? shortPrice : largePrice);
				}
			}

			if (data.price_variation && data.price_variation?.percentage < 50 && !isTemp) {
				setPriceVariation({
					text: makePriceText(data.price_variation),
					...data.price_variation,
				});
			}

			if (data.commonExpenses && (!isProject || forceProperty) && !isTemp) {
				const commonExpensesText = `+ ${makePriceText(data.commonExpenses)} ${
					shortExpensesText ? t("gc") : t("gastos comunes")
				}`;
				setCommonExpenses({ ...data.commonExpenses, text: commonExpensesText });
			}

			if (isTemp) {
				setPrice({ amount: data?.temporal_price, isTemp });
				setPriceText(`${data?.temporal_currency?.name} ${formatMoney(Math.round(data?.temporal_price))}`);
			}
		}
	}, [data]);

	return {
		loading,
		data: {
			price: {
				...price,
				text: priceText,
			},
			commonExpenses,
			operationType: {
				text: operationType,
			},
			price_variation: priceVariation,
		},
		error,
	};
}
