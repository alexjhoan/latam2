import React from "react";
import { usePriceTag } from "./PriceTag.hook";
import { Text } from "react-native";

export function PriceTag({ id }) {
  const { data, loading, error } = usePriceTag({ id });

  if (loading) return null;
  if (error) return null;
  return (
    <>
      <Text>{data.price.text}</Text>
      {!data.commonExpenses.hidePrice && (
        <Text>{data.commonExpenses.text}</Text>
      )}
    </>
  );
}
