import { useEffect, useState } from "react";
import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";

export const FRAGMENT_FACILITIES = new FragmentDefiner(
	"Property",
	`
    facilities {
      name
      group
    }
    project {
      facilities
    }
`
);

const parseProjectFacilities = (facilities) => {
  return facilities.map(facility => ({ name: facility }))
}

export function useFacilities({ id, mode }) {
  const { loading, data, error } = useReadFragment(FRAGMENT_FACILITIES, id);
  const [facilities, setFacilities] = useState([])

  useEffect(() => {
    if(data?.project.length > 0 && mode === 'project') {
      setFacilities(parseProjectFacilities(data.project[0].facilities))
    } else if(data?.ficilities) {
      setFacilities(data.facilities)
    }
  }, [data])

	return { loading, facilities, error };
}
