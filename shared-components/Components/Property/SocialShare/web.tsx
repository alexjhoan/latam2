import "./styles.less";

import { Button, Form, Input, Modal, Space, Typography, notification } from "antd";
import {
	CloseCircleOutlined,
	FacebookFilled,
	InstagramFilled,
	LinkedinFilled,
	ShareAltOutlined,
	TwitterSquareFilled,
	YoutubeFilled,
} from "@ant-design/icons";
import React, { useEffect, useState } from "react";

import { PropComponentMode } from "../PropertyInterfaces";
import { Rule } from "antd/lib/form";
import { removeUrlProtocol } from "../../../Utils/Functions";
import { useSocialShare } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { useUser } from "../../User/User.hook";

const { Title, Link } = Typography;

const socialMediaIcons = Object.freeze({
	YouTube: <YoutubeFilled />,
	Facebook: <FacebookFilled />,
	Twitter: <TwitterSquareFilled />,
	LinkedIn: <LinkedinFilled />,
	Instagram: <InstagramFilled />,
});

export function SocialShare({ id, mode = "auto" }: { id: any; mode?: PropComponentMode }) {
	const { loading, error, socialMediaLinks } = useSocialShare({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();
	const { isLoggedIn, user } = useUser();

	const [modal, setModal] = useState(false);
	const [form] = Form.useForm();

	const onCreate = values => {
		notification.success({
			message: t("Propiedad Compartida"),
			description: t(
				"La propiedad ha sido compartida exitosamente. Le enviamos esta propiedad a " +
					values.other
			),
			style: { maxWidth: "calc(100% - 16px)" },
		});
		setModal(false);
	};

	const socialShare = socialName => {
		const socialLink = socialMediaLinks.find(({ name }) => name === socialName);
		return removeUrlProtocol(socialLink.url);
	};
	const shareTwitter = socialName => {
		const socialLink = socialMediaLinks.find(({ name }) => name === socialName);
		const link = socialLink.url.replace("home?status=", "intent/tweet?url=");
		return link;
	};

	useEffect(() => {
		if (isLoggedIn) form.setFieldsValue({ me: user.data?.me.email });
	}, [isLoggedIn, user]);

	const emailRules: Rule[] = [
		{ required: true, message: "Ingrese el correo electrónico." },
		{ type: "email", message: "Ingrese un correo válido." },
	];

	const CollectionCreateForm = ({ visible, onCreate, onCancel }) => {
		return (
			<Modal
				className="social-share-modal"
				visible={visible}
				footer={null}
				closeIcon={<CloseCircleOutlined />}
				onCancel={onCancel}
				title={t("Compartir Propiedad")}
				centered
				width="100%">
				<Form
					onFinish={onCreate}
					requiredMark={false}
					form={form}
					layout="vertical"
					name="form_in_modal"
					scrollToFirstError>
					<Form.Item
						name="other"
						label={t("Comparte esta propiedad con") + ": "}
						rules={emailRules}>
						<Input placeholder="Correo electrónico" />
					</Form.Item>
					<Form.Item name="me" label={t("Tu") + ": "} rules={emailRules}>
						<Input placeholder="Tu correo electrónico" />
					</Form.Item>
					<Space>
						<Form.Item>
							<Button type="primary" loading={loading} htmlType={"submit"}>
								{t("Compartir")}
							</Button>
						</Form.Item>
						<Form.Item>
							<Button onClick={() => setModal(false)}>{t("Cancelar")}</Button>
						</Form.Item>
					</Space>
					<Form.Item className="social-share-networks">
						<Title level={4}>{t("O tambien puedes compartir en") + ":"}</Title>
						{socialMediaLinks.length > 0 && (
							<Space>
								<Button
									className="secondary"
									href={socialShare("facebook")}
									target={"_blank"}
									icon={socialMediaIcons.Facebook}
								/>
								<Button
									className="secondary"
									href={shareTwitter("twitter")}
									target={"_blank"}
									icon={socialMediaIcons.Twitter}
								/>
							</Space>
						)}
					</Form.Item>
				</Form>
			</Modal>
		);
	};

	return (
		<>
			<div>
				<Button
					disabled={loading}
					className={"social-share-button secondary"}
					onClick={e => {
						e.stopPropagation();
						setModal(true);
					}}
					icon={<ShareAltOutlined />}>
					{t("Compartir")}
				</Button>
				<CollectionCreateForm
					visible={modal}
					onCreate={onCreate}
					onCancel={() => setModal(false)}
				/>
			</div>
			<style jsx global>{`
				.social-share-modal .ant-modal-title {
					font-size: ${theme.fontSizes.lgTitle};
					line-height: ${theme.fontSizes.lgLineHeight};
				}
				.social-share-modal label {
					color: ${theme.colors.textSecondaryColor};
				}
				.social-share-modal .social-share-networks {
					margin-top: ${theme.spacing.xxlSpacing}px;
				}
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.property-detail .social-share-button .anticon {
						color: ${theme.colors.backgroundColor};
					}
					.social-share-button:hover svg {
						color: ${theme.colors.secondaryColor};
					}
				}
			`}</style>
		</>
	);
}
