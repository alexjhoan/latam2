import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { useEffect, useState } from "react";

export const FRAGMENT_SOCIAL_SHARE = new FragmentDefiner(
	"Property",
	`
    socialMediaLinks {
      name
      url
    }
    project {
        socialMediaLinks {
            name
            url
        }
    }
`
);

export const useSocialShare = ({ id, mode }) => {
	const { loading, data, error } = useReadFragment(FRAGMENT_SOCIAL_SHARE, id);
	const [socialMedia, setSocialMedia] = useState([]);

	useEffect(() => {
		if ((data?.project[0] && mode == "auto") || mode == "project") {
			if (data?.project[0].socialMediaLinks.length > 0) {
				setSocialMedia(data.project[0].socialMediaLinks);
			}
		} else {
			if (data?.socialMediaLinks.length > 0) {
				setSocialMedia(data.socialMediaLinks);
			}
		}
	}, [data]);

	return {
		loading,
		error,
		socialMediaLinks: socialMedia,
	};
};
