import * as React from "react";
import { useState, MouseEvent, useEffect } from "react";
import { useTranslation } from "react-i18next";
import { Button } from "antd";
import { PhoneOutlined } from "@ant-design/icons";
import { ModalPhone } from "../../../ModalPhone/web";
import { usePhoneButton, OwnerPhoneInterface } from "./hook";
import { useTheme } from "../../../../../Styles/ThemeHook";
import { useGoogleAnalytics } from "../../../../../GlobalHooks/web/GoogleAnalytics.hook";

export const PhoneButton = ({ id, type = "callButton", mode }: OwnerPhoneInterface) => {
	const { t } = useTranslation();
	const GA = useGoogleAnalytics();
	const { theme } = useTheme();
	const { viewPhone, masked_phone, phone, owner_name, loading } = usePhoneButton({
		property_id: id,
	});

	const [modal, setModal] = useState(false);
	useEffect(() => {
		if (modal) GA.Event({ category: "view_phone", action: "view_phone ic2" });
	}, [modal]);

	const handleClick = (e: MouseEvent) => {
		e.preventDefault();
		e.stopPropagation();
		if (!phone) {
			viewPhone().then(data => {
				if (data) setModal(true);
			});
		} else {
			setModal(true);
		}
	};

	const viewPhoneBtn = <Button onClick={handleClick}>{t("Ver") + " " + masked_phone}</Button>;
	const callBtn = (
		<Button
			loading={loading}
			onClick={handleClick}
			style={{ borderColor: theme.colors.linkColor }}
			icon={<PhoneOutlined style={{ color: theme.colors.linkColor }} />}
		/>
	);

	return (
		<>
			{type == "callButton" ? callBtn : viewPhoneBtn}
			<ModalPhone
				visible={modal}
				loading={loading}
				onCancel={() => setModal(false)}
				property_id={id}
				owner_name={owner_name}
				phone={phone}
				mode={mode}
			/>
		</>
	);
};
