import { gql } from "@apollo/client/core";
import { useMutation } from "@apollo/client";
import { FragmentDefiner, useReadFragment } from "../../../../../GlobalHooks/useReadFragment";
import { PropComponentMode } from "../../../PropertyInterfaces";

const REQUEST_PHONE_MUTATION = gql`
	mutation request_phone_mutation($property_id: Int!) {
		requestPhone(property_id: $property_id)
	}
`;

export interface OwnerPhoneInterface {
	id: number | string;
	type?: "callButton" | "viewPhone";
	mode: PropComponentMode;
}

export const FRAGMENT_MASKED_PHONE = new FragmentDefiner(
	"Property",
	`
    owner {
        id
        name
        masked_phone
    }
`
);

export const usePhoneButton = ({ property_id }) => {
	const [mutation, { loading, data: mutationData }] = useMutation(REQUEST_PHONE_MUTATION, {
		onError: error => {},
	});
	const { data } = useReadFragment(FRAGMENT_MASKED_PHONE, property_id);

	//nota: como el telefono se guarda en un state, no en cache de apolo. cada ver teléfono va a hacer la mutation por separado
	const viewPhone = () => {
		return mutation({
			variables: {
				property_id,
			},
		});
	};

	return {
		viewPhone,
		owner_name: data?.owner.name,
		masked_phone: data?.owner.masked_phone,
		phone: mutationData?.requestPhone,
		loading,
	};
};
