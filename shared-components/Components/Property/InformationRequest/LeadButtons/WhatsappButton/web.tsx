import { Button } from "antd";
import { useWhatsppButton } from "./hook";
import { WhatsAppOutlined } from "@ant-design/icons";
import { useGoogleAnalytics } from "../../../../../GlobalHooks/web/GoogleAnalytics.hook";

export const WhatsappButton = ({ id }) => {
	const { error, hasWhatsapp, getWppNumber, wppMessage } = useWhatsppButton(id);
	const GA = useGoogleAnalytics();

	const clickHandler = () => {
		getWppNumber().then((res:any) => { 
			if(res?.data && res?.data?.requestPhone){
				GA.Event({ category: "wpp_request", action: "wpp_request ic2" });
				let url = `https://api.whatsapp.com/send?phone=${res?.data?.requestPhone}&text=${wppMessage}`;
           	 	window.open(url)
			}
		})
	}

	if (!hasWhatsapp || error) return null;

	return (
		<>
			<Button
				onClick={clickHandler}
				icon={<WhatsAppOutlined style={{ color: "#00C841" }} />}
				style={{ borderColor: "#00C841" }}
			/>
		</>
	);
};
