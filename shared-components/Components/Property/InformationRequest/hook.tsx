import { useState, useEffect } from "react";
import { useAuthCheck } from "../../User/useAuthCheck";
import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { useUser } from "../../User/User.hook";
import { USER_EXISTS_QUERY, REGISTER_MUTATION } from "../../User/User.querys";
import { gql } from "@apollo/client/core";
import { useMutation, useLazyQuery } from "@apollo/client";
import cookie from "js-cookie";
import { openAuthModal } from "../../User/AuthModal/web";
import { PropComponentMode } from "../../Property/PropertyInterfaces";

interface useContactParams {
	id: number;
	onComplete: Function;
	type: string;
	mode: PropComponentMode;
}

enum VisitType {
	INPERSON = "INPERSON",
	VIRTUAL = "VIRTUAL",
}

enum PropEntityType {
	PROPERTY = "PROPERTY",
	PROJECT = "PROJECT",
}

interface FormData {
	email: string;
	phone: number | string;
	name: string;
	message?: string;
	financing_information?: boolean | undefined;
	date?: string;
	visit_type?: VisitType;
}

interface ContactInput {
	property_id: number;
	source: number;
	type: PropEntityType;
	financing_information: boolean;
	message?: string;
	visit_type?: string;
	date?: string;
}

export const FRAGMENT_CONTACT_FORM = new FragmentDefiner(
	"Property",
	`
  project {
    id
  }
`
);

const SEND_LEAD_MUTATION = gql`
	mutation sendLead($input: LeadInput!) {
		sendLead(input: $input) {
			id
		}
	}
`;

const SCHEDULE_VISIT_MUTATION = gql`
	mutation scheduleVisit($input: ScheduleInput!) {
		scheduleVisit(input: $input) {
			id
		}
	}
`;

const UPDATE_USER_MUTATION = gql`
	mutation updateUser($name: String, $phone: String) {
		updateUser(name: $name, phone: $phone) {
			id
		}
	}
`;

export const useContact = ({ id, onComplete, type, mode = "auto" }: useContactParams) => {
	const [formData, setFormData] = useState<FormData | null>(null);
	const [formComplete, setFormComplete] = useState(false);
	const { isLoggedIn, recheck } = useAuthCheck();
	const { data } = useReadFragment(FRAGMENT_CONTACT_FORM, id);
	const { user } = useUser();

	const [contactMutation, { loading: contactLoading, data: contactData }] = useMutation(
		type == "lead" ? SEND_LEAD_MUTATION : SCHEDULE_VISIT_MUTATION,
		{ onCompleted: () => setFormComplete(true) }
	);
	const [registerMutation, { loading: registerLoading }] = useMutation(REGISTER_MUTATION);
	const [updateUserMutation, { loading: updateLoading }] = useMutation(UPDATE_USER_MUTATION);
	const [userExistsQuery, { loading: userExistsLoading }] = useLazyQuery(USER_EXISTS_QUERY, {
		onCompleted: data => {
			openAuthModal({ withPromise: true, email: formData.email })
				.then(({ data }) => {
					updateUser({ name: formData.name, phone: formData.phone }).then(data => {
						doContactMutation();
					});
				})
				.catch(error => {
					//if(onComplete) onComplete();
				});
		},
		onError: error => {
			if (error.message.includes("no existe")) {
				registerMutation({
					variables: {
						email: formData.email,
						name: formData.name,
					},
				})
					.then(({ data }) => {
						refreshAuth(data.registerIndividual);
						updateUser({ name: formData.name, phone: formData.phone }).then(() => {
							doContactMutation();
						});
					})
					.catch(error => {
						console.error(error);
					});
			}
		},
	});

	useEffect(() => {
		if (formData) {
			if (isLoggedIn) {
				updateUser({ name: formData.name, phone: formData.phone }).then(data => {
					user.refetch();
					doContactMutation();
				});
			} else {
				userExistsQuery({ variables: { email: formData.email } });
			}
		}
	}, [formData]);

	const updateUser = ({ name, phone }) => {
		return updateUserMutation({ variables: { name, phone } });
	};

	const doContactMutation = () => {
		let property_id: number;
		let propertyType: PropEntityType;
		if ((data.project.length && mode == "auto") || mode == "project") {
			property_id = data.project[0].id;
			propertyType = PropEntityType.PROJECT;
		} else {
			property_id = id;
			propertyType = PropEntityType.PROPERTY;
		}

		let contactInput: ContactInput = {
			property_id,
			source: 0,
			type: propertyType,
			financing_information: formData.financing_information,
		};

		if (type == "lead") {
			contactInput.message = formData.message;
		} else {
			contactInput.date = formData.date;
			contactInput.visit_type = formData.visit_type;
		}

		contactMutation({
			variables: {
				input: contactInput,
			},
		})
			.then(data => {
				if (onComplete) onComplete();
			})
			.catch(error => {
				console.error(error);
			});
	};

	const refreshAuth = data => {
		cookie.set("frontend_token", data.access_token);
		localStorage.setItem("user_md5", data.user_md5);
		recheck();
	};

	const sendContact = data => {
		const newFormData: FormData = {
			email: data.email,
			name: data.nombre,
			phone: data.telefono,
			message: data?.mensaje,
			financing_information: !!data.financing_information,
			date: data?.date
				? `${data.date.getFullYear()}-${data.date.getMonth() + 1}-${data.date.getDate()}`
				: null,
			visit_type: data?.visit_type == "INPERSON" ? VisitType.INPERSON : VisitType.VIRTUAL,
		};
		setFormData(newFormData);
	};

	const resetForm = () => {
		setFormComplete(false);
		setFormData(null);
	};

	return {
		loading: contactLoading || registerLoading || updateLoading || userExistsLoading,
		formCompleted: formComplete,
		resetForm: resetForm,
		sendContact,
		disabledEmail: isLoggedIn,
	};
};
