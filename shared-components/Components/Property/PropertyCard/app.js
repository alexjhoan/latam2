import React from 'react'
import {usePropertyCard} from "./PropertyCard.hook";
import {PriceTag} from "../PriceTag/app";
import { Text, View } from "react-native";

export function PropertyCard({id}) {
    const {data,loading,error} = usePropertyCard({id})



    if (loading) {
        return (<Text>Loading Card...</Text>)
    }

    return (
        <View>
            <Text>Propiedad {data.title} del tipo {data.property_type_id.name} </Text>
            <PriceTag id={id}/>
        </View>
    );
}
