import "./styles.less";

import { Avatar, Skeleton, Space, Typography } from "antd";

import React from "react";
import { useOwnerLogo } from "./hook";
import { useTheme } from "../../../Styles/ThemeHook";

const { Text } = Typography;

export const OwnerLogo = ({ id, type = "small" }) => {
	const { loading, error, owner } = useOwnerLogo({ id });
	const { theme } = useTheme();

	if (loading) return <OwnerLogoSkeleton />;
	if (error) return null;
	if (!owner) return null;

	return (
		<>
			<a href={owner.inmoLink || owner.inmoPropsLink} target="_blank">
				<div className="property-owner-logo">
					<Space>
						<Avatar
							src={owner.logo}
							alt={owner.name}
							children={owner.name}
							size={"large"}
						/>
						{type == "full" && <Text strong>{owner.name}</Text>}
					</Space>
				</div>
			</a>
			<style jsx global>{`
				.property-card .property-owner-logo {
					right: ${theme.spacing.smSpacing}px;
					top: ${theme.spacing.smSpacing}px;
				}
				.property-owner-logo .ant-avatar-lg {
					border-color: ${theme.colors.borderColor};
				}
			`}</style>
		</>
	);
};

export const OwnerLogoSkeleton = () => {
	return <Skeleton.Avatar active></Skeleton.Avatar>;
};
