import "./styles.less";

import { Skeleton, Space, Typography } from "antd";

import { BathroomsTag } from "./BathroomsTag/web";
import { BedroomsTag } from "./BedroomsTag/web";
import { GuestsTag } from "./GuestsTag/web";
import { M2Tag } from "./M2Tag/web";
import { RoomsTag } from "./RoomsTag/web";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTypologyTag } from "./TypologyTag.hook";

export function TypologyTag({ id, mode = "auto", showIcons = false, className = "" }) {
	const {
		data: { showBathrooms, showBedrooms, showM2, showRooms, showGuests },
		loading,
		error,
	} = useTypologyTag({ id });
	const { theme } = useTheme();

	if (loading) return <TypologyTagSkeleton className={className} />;

	if (error) return null;

	const separador = !showIcons ? <Typography.Text>·</Typography.Text> : null;
	return (
		<>
			<Space
				align={"baseline"}
				size={showIcons ? theme.spacing.lgSpacing : theme.spacing.xsSpacing}
				className={
					"property-typology-tag" + " " + className + (showIcons ? " show-icons" : "")
				}>
				{showBedrooms && <BedroomsTag id={id} mode={mode} icon={showIcons} />}

				{showRooms && showBedrooms && separador}
				{showRooms && <RoomsTag id={id} mode={mode} icon={showIcons} />}

				{showBathrooms && (showBedrooms || showRooms) && separador}
				{showBathrooms && <BathroomsTag id={id} mode={mode} icon={showIcons} />}

				{showM2 && (showBedrooms || showRooms || showBathrooms) && separador}
				{showM2 && <M2Tag id={id} mode={mode} icon={showIcons} />}

				{showGuests && (showBedrooms || showRooms || showBathrooms || showM2) && separador}
				{showGuests && <GuestsTag id={id} mode={mode} icon={showIcons} />}
			</Space>
			<style jsx global>{`
				// NO BORRAR, ESTAMOS PROBANDO 
				// .property-card .property-typology-tag {
				// 	font-size: ${theme.fontSizes.smFontSize};
				// 	line-height: ${theme.fontSizes.baseLineHeight};
				// }
				.property-card .property-typology-tag {
					font-size: ${theme.fontSizes.baseFontSize};
					line-height: ${theme.fontSizes.baseLineHeight};
				}
			`}</style>
		</>
	);
}

export const TypologyTagSkeleton = ({ className }) => {
	return (
		<Space align={"center"} className={"property-typology-tag" + " " + className}>
			<TagSkeleton />
			<TagSkeleton />
			<TagSkeleton />
		</Space>
	);
};

export const TagSkeleton = () => {
	return <Skeleton.Button size={"small"} active />;
};
