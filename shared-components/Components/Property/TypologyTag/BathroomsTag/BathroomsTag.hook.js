import {FragmentDefiner, useReadFragment} from "../../../../GlobalHooks/useReadFragment";
import {showBathrooms} from "../../../../Utils/Functions";

export const FRAGMENT_BATHROOMSTAG = new FragmentDefiner("Property",`
    bathrooms
    project {
        bathrooms
        bathrooms_listing
    }
    property_type {
        id
    }
    operation_type {
        id
    }
`);

export function useBathroomsTag({id, mode = "auto"}) {
    const {loading, data, error} = useReadFragment(FRAGMENT_BATHROOMSTAG,id);

    let text = '';
    if(!loading) {
        if((mode === "auto" && data.project[0]) || mode === "project"){
            text = textProject(data, true);
        } else {
            text = textProperty(data);
        }
    }

    return { loading, text, error }
}

function textProperty(data){
    return data.bathrooms + ' Baño' + (data.bathrooms > 1 ? 's':'')
}

function textProject(data, short){
    if(short){
        if(data.project[0].bathrooms.length > 1){
            if(data.project[0].bathrooms_listing){
                return `+${data.project[0].bathrooms_listing[0]} Baños`;
            } else {
                return `+${data.project[0].bathrooms[0]} Baños`;
            }
        } else {
            return textProperty(data);
        }
    }
}