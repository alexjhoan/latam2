import { Space, Typography } from "antd";

import React from "react";
import { SquareM2Icon } from "../../../CustomIcons/web";
import { TagSkeleton } from "../web";
import { useM2Tag } from "./M2Tag.hook";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

export function M2Tag({ id, mode = "auto", icon = false }) {
	const { text, loading, error, show } = useM2Tag({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (error || !show) return null;
	if (loading) return <TagSkeleton />;

	return (
		<Space size={theme.spacing.smSpacing}>
			{icon && <SquareM2Icon style={{ color: theme.colors.borderColor }} />}
			<Typography.Text ellipsis>{t(text.replace(/m2/gi, "m²"))}</Typography.Text>
		</Space>
	);
}
