import {FragmentDefiner, useReadFragment} from "../../../../GlobalHooks/useReadFragment";

export const FRAGMENT_GUESTSTAG = new FragmentDefiner("Property",`
    guests 
    project {
        minGuests
    }
`);

export function useGuestsTag({id, mode = "auto"}) {
    const {loading, data, error} = useReadFragment(FRAGMENT_GUESTSTAG, id);

    let text = '';
    if(!loading) {
        if((mode == "auto" && data.project[0]) || mode == "project"){
            if (data.project[0].minGuests != null){
                text = "+" + data.project[0].minGuests + " huesped" + (data.guests > 1 ? 'es' : '');
            }
        } else {
            if (data.guests && data.guests > 0) {
               text = data.guests +  ' huesped' + (data.guests > 1 ? 'es':'');
            }
        }
    }

    return { loading, text, error }
}