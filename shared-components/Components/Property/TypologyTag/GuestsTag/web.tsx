import { useGuestsTag } from "./GuestsTag.hook";
import { Space, Typography } from "antd";
import { TagSkeleton } from "../web";
import { useTranslation } from "react-i18next";
import React from "react";
import { useTheme } from "../../../../Styles/ThemeHook";
import { UserOutlined } from "@ant-design/icons";

export function GuestsTag({ id, mode = "auto", icon = false }) {
	const { text, loading, error } = useGuestsTag({ id, mode });
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (error || text == "") return null;
	if (loading) return <TagSkeleton />;

	return (
		<Space size={theme.spacing.smSpacing}>
			{icon && <UserOutlined style={{ color: theme.colors.borderColor }} />}
			<Typography.Text ellipsis>{t(text)}</Typography.Text>
		</Space>
	);
}
