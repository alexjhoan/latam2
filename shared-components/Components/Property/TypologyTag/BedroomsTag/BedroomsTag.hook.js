import {FragmentDefiner, useReadFragment} from "../../../../GlobalHooks/useReadFragment";
import {showBedrooms} from "../../../../Utils/Functions";

export const FRAGMENT_BEDROOMSTAG = new FragmentDefiner("Property",`
    operation_type {
        id
    }
    bedrooms
    project {
        bedrooms
        bedrooms_listing
    }
    
    property_type {
        id
    }
`);

export function useBedroomsTag({id, mode = "auto"}) {
    const {loading, data, error} = useReadFragment(FRAGMENT_BEDROOMSTAG, id);

    let text = '';
    if (!loading) {
        if((mode === "auto" && data.project[0]) || mode === "project") {
            text = parseTextProject(data, true);
        } else {
            text = parseTextProperty(data);
        }
    }

    return {loading, text, error}
}

function parseTextProperty(data) {
    if (data.bedrooms === 0) {
        return monoText
    } else {
        return (data.bedrooms) + ' ' + (data.bedrooms > 1 ? pluralBedRoomText : bedRoomText)
    }
}

function parseTextProject(data, short) {
    if (short){
        if(data.project[0].bedrooms.length > 1){
            if(data.project[0].bedrooms_listing){
                return `+${data.project[0].bedrooms_listing[0]-1 > 0 ? data.project[0].bedrooms_listing[0]-1 : 1} ${pluralBedRoomText}`;
            } else {
                return `+${data.project[0].bedrooms[0] > 0 ? data.project[0].bedrooms[0] : 1} ${pluralBedRoomText}`;
            }
        } else {
            return parseTextProperty(data);
        }
    }
}

const monoText = "Mono"; // "Monoambiente"
const bedRoomText = "Dorm.";  // "Dormitorio"
const pluralBedRoomText = "Dorm."; // "Dormitorios"