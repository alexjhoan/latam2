import {
	FragmentDefiner,
	useReadFragment,
} from "../../../../GlobalHooks/useReadFragment";
import { showRooms } from "../../../../Utils/Functions";

export const FRAGMENT_ROOMSTAG = new FragmentDefiner(
	"Property",
	`
    operation_type {
        id
    }
    property_type {
        id
    }
		rooms
		project {
			rooms
		}
`
);

export function useRoomsTag({ id, mode = "auto" }) {
	const { loading, data, error } = useReadFragment(FRAGMENT_ROOMSTAG, id);

	let text = "";
	if (!loading) {
		if ((mode === "auto" && data?.project[0]) || mode === "project") {
			text = parseTextProject(data, true);
		} else {
			text = parseTextProperty(data);
		}
	}

	return { loading, text, error};
}

function parseTextProperty(data) {
	return data.rooms + " Ambiente" + (data.rooms > 1 ? "s" : "");
}

function parseTextProject(data, short) {
	if(short){
		if (data.project[0].rooms.length > 1){
			return `+${data.project[0].rooms[0]} Ambientes`;
		} else {
			return parseTextProperty(data);
		}
	}
}
