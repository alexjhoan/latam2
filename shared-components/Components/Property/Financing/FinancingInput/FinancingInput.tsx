import { Row, Col, Typography } from "antd";
import { useTheme } from "../../../../Styles/ThemeHook";

export const FinancingInput = ({title, input, slider = null, bottom = null, centerBottom = null, fullWidth = false}) => {
  const { theme } = useTheme();
  
  return (
    <div className="financing-col">
      <Row className="inputs-col">
        <Col xs={24} md={fullWidth ? 24 : 12}>
          <Typography.Paragraph strong>{title}</Typography.Paragraph>
        </Col>
        <Col xs={24} md={fullWidth ? 24 : 12}>
          {input}
        </Col>
        { slider ? 
          <Col xs={24}>
            {slider}
          </Col> : null  
        }
        { bottom ? 
          <Col xs={24} style={centerBottom ? {textAlign: "center"} : null}>
            {bottom}
          </Col> : null
        }

      </Row>
      <style jsx global>{`
        .financing-col .ant-slider-track {
          background-color: ${theme.colors.secondaryColor}
        }
        .financing-col .ant-slider-handle {
          border: solid 4px #fff;
          width: 16px;
          height: 16px;
          background-color: ${theme.colors.secondaryColor}
        }
        .financing-small-text {
          font-size: .8em;
        }
      `}</style>
    </div>
  )
}