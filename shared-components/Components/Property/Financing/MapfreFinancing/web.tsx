import { useState } from "react";
import { useTheme } from "../../../../Styles/ThemeHook";
import { Row, Col, Typography, Button, Skeleton, message, notification } from "antd";
import { Input } from "../dataInputs";
import { FinancingInput } from "../FinancingInput/FinancingInput";
import { ImagenOptimizada } from "../../../Image/web";
import "./styles.less";
import { useTranslation } from "react-i18next";
import { ContactModal } from "../ContactModal/web";
import { useMapfreFinancing } from "./hook";
import { formatMoney, isCommerce } from "../../../../Utils/Functions";

const LEAD_SERVICE = "https://prod.infocasas.com.uy/microservices/simulador/?/mapfre/sendForm";

export const MapfreFinancing = ({ id, propertyType }) => {
	const { t } = useTranslation();
	const { theme } = useTheme();
	const [modal, setModal] = useState(false);
	const {
		priceAmount: { value: priceAmount, change: setPriceAmount },
		currency,
		minimumIncome,
		annualFees,
		cash,
		loading,
		extraData,
	} = useMapfreFinancing({ property_id: id });

	return (
		<div className="property-financing-mapfre">
			<div className="mapfre-title">
				<Typography.Title level={4}>{t("Garantía de Alquiler")}</Typography.Title>
			</div>
			<Row align={"middle"} gutter={[0, { xs: theme.spacing.lgSpacing }]}>
				<Col xs={24} md={12}>
					<div className="property-financing-box">
						<div className="mapfre-logo">
							<ImagenOptimizada
								src="https://cdn2.infocasas.com.uy/web/5eecaacba7a8d_infocdn__mapfre-blancosinfondo.png"
								alt="MAPFRE"
							/>
						</div>
						<Row gutter={[theme.spacing.xxlSpacing, theme.spacing.mdSpacing]}>
							<Col xs={24}>
								<FinancingInput
									title="Importe de Alquiler"
									input={
										<Input
											value={priceAmount}
											onChange={x => {
												setPriceAmount(x);
											}}
											currency={currency}
										/>
									}
									fullWidth
								/>
							</Col>
							<Col xs={24}>
								<FinancingInput
									title="Ingresos necesarios para este alquiler."
									input={
										loading ? (
											<Skeleton.Button size="small" active />
										) : (
											<Typography.Paragraph>
												{currency} {formatMoney(minimumIncome)}
											</Typography.Paragraph>
										)
									}
									bottom={
										<Typography.Paragraph className="financing-small-text">
											{isCommerce(propertyType)
												? "Para alquileres comerciales, el alquiler máximo asegurable no podrá ser mayor al 30% sobre utilidades"
												: "El ingreso liquido requerido puede ser compuesto por los ingresos de hasta 3 personas."}
										</Typography.Paragraph>
									}
									fullWidth
								/>
							</Col>
						</Row>
					</div>
				</Col>
				<Col xs={24} md={12} className="property-financing-results">
					<Typography.Title>Garantía</Typography.Title>
					<Row>
						<Col xs={isCommerce(propertyType) ? 24 : 12}>
							<Typography className="financing-subprice">
								{isCommerce(propertyType)
									? "Costo anual de la Garantía"
									: "Contado"}
							</Typography>
							{loading ? (
								<Skeleton.Button size="small" active />
							) : (
								<Typography className="financing-price">
									{currency} {formatMoney(cash)}
								</Typography>
							)}
						</Col>
						{isCommerce(propertyType) ? null : (
							<Col xs={12}>
								<Typography className="financing-subprice">
									{isCommerce(propertyType)
										? "Costo anual de la Garantía"
										: "¡12 cuotas sin recargo!"}
								</Typography>
								{loading ? (
									<Skeleton.Button size="small" active />
								) : (
									<Typography className="financing-price">
										{currency} {formatMoney(annualFees)}
									</Typography>
								)}
							</Col>
						)}
					</Row>
					<Typography className="financing-subprice bottom">
						{isCommerce(propertyType)
							? "Consulte por opciones de financiación"
							: "Consulta por las promociones vigentes con tarjeta de crédito"}
					</Typography>
					<Button className="mapfre-financing-button" onClick={() => setModal(true)}>
						Solicitar más información
					</Button>
				</Col>
			</Row>
			<ContactModal
				sendLeadURL={LEAD_SERVICE}
				extraData={extraData}
				onSuccess={() => {
					setModal(false);
					notification.success({
						message: <b>{t("¡Gracias por tu consulta!")}</b>,
						description: t(
							"Tus datos han sido enviados con éxito. En las próximas horas nos contacataremos contigo para darte más información."
						),
						style: { maxWidth: "calc(100% - 16px)" },
					});
				}}
				showCI
				title="¡Solicitar mi Garantía!"
				topText="Completá los datos que faltan y uno de nuestros agentes se contactará contigo"
				bottomText={
					isCommerce(propertyType)
						? [
								"Para alquileres comerciales, el alquiler máximo asegurable no podrá ser mayor al 30% sobre utilidades",
								"El tiempo máximo de análisis será de 48 hs.",
						  ]
						: [
								"El importe máximo a garantizar será de hasta el 30% del ingreso líquido del solicitante.",
								"Se podrá hacer composición de ingresos de hasta tres personas en total, las cuales serán co-titulares del contrato de arrendamiento y de la póliza de seguro.",
								"El tiempo máximo de análisis será de 48 hs.",
								"El costo detallado es independiente de la cantidad de solicitantes que se incorporen al estudio.",
						  ]
				}
				visible={modal}
				closeModal={() => setModal(false)}
			/>
		</div>
	);
};
