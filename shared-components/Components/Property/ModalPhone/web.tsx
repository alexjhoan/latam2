import { useTranslation } from "react-i18next";
import { useIsAuthModalShowing } from "../../../GlobalHooks/useIsAuthModalShowing";
import { ModalSimilars } from "../ModalSimilars/web";
import { Row, Col, Typography, Space } from "antd";
import { CheckCircleTwoTone, PhoneOutlined } from "@ant-design/icons";
import { useTheme } from "../../../Styles/ThemeHook";

export const ModalPhone = ({
	visible,
	onCancel,
	loading,
	owner_name,
	phone,
	property_id,
	mode,
}) => {
	const { t } = useTranslation();
	const { theme } = useTheme();
	const { isAuthModalShowing } = useIsAuthModalShowing();

	let phones = [];
	if (typeof phone != "undefined") phones = phone.split(";");

	return (
		<>
			<ModalSimilars
				mode={mode}
				visible={visible && !isAuthModalShowing}
				onCancel={onCancel}
				property_id={property_id}>
				<Row
					className={"modal-phone-content"}
					justify={"center"}
					style={{ textAlign: "center" }}
					gutter={[0, theme.spacing.xsSpacing]}>
					<Col span={24}>
						<CheckCircleTwoTone twoToneColor="#52c41a" style={{ fontSize: "25px" }} />
					</Col>
					<Col span={24}>
						<Typography.Text strong>
							{t("¡No pierdas tiempo y llamá ahora!")}
						</Typography.Text>
					</Col>
					<Col span={24}>
						<Typography.Text strong>{loading ? "loading" : owner_name}</Typography.Text>
					</Col>
					<Col span={24}>
						<Row gutter={theme.spacing.lgSpacing} justify={"center"}>
							{phones.map((p, i) => (
								<Col key={"phone_" + i}>
									<a href={"tel:" + p}>
										<Space size={theme.spacing.xsSpacing}>
											<PhoneOutlined
												style={{ color: theme.colors.linkColor }}
											/>
											<Typography.Text
												style={{ color: theme.colors.linkColor }}>
												{loading ? "loading" : p}
											</Typography.Text>
										</Space>
									</a>
								</Col>
							))}
						</Row>
					</Col>
				</Row>
			</ModalSimilars>
			<style jsx>{``}</style>
		</>
	);
};
