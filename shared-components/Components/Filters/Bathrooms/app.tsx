import { ButtonSelect } from "../../Inputs/app";
import { useBathrooms } from "./Bathrooms.hook";

import { Label } from "../../Inputs/Label/app";
import React, { Component } from "react";
import { View, Text } from "react-native";
import { FilterComponentType } from "../Filters.hook";

const Bathrooms = (props: FilterComponentType) => {
	const { loading, data, error, show, label, labeled } = useBathrooms(props);

	if (!show) return null;
	if (loading) return <Text>loading</Text>;
	if (error) return <Text>error</Text>;

	return (
		<View>
			<Label labelContent={label} showLabel={labeled}>
				<ButtonSelect {...data} />
			</Label>
		</View>
	);
};

export { Bathrooms };
