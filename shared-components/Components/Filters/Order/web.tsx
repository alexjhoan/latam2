import "./styles.less";

import { Grid, Select, Skeleton } from "antd";

import { OrderIcon } from "../../CustomIcons/web";
import { useOrder } from "./Order.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Option } = Select;
const { useBreakpoint } = Grid;

export const Order = () => {
	const {
		loading,
		data: { options, customKey, valueKey, value, onChange },
		error,
		show,
	} = useOrder();

	const { t } = useTranslation();
	const { theme } = useTheme();
	const screens = useBreakpoint();

	if (!show) return null;
	if (error) return <div>{t("error")}</div>;
	if (loading) return <Skeleton.Button size="large" style={{ height: "40px" }} active />;

	const handleChange = e => {
		const res = options.find(o => o[customKey] == e);
		onChange(res);
	};

	return (
		<>
			<div className="filter order-filter">
				<Select
					dropdownMatchSelectWidth={false}
					dropdownAlign={!screens.lg && { offset: ["-85", 4] }}
					disabled={loading}
					value={value}
					onChange={handleChange}
					suffixIcon={
						<OrderIcon
							style={{
								fontSize: theme.fontSizes.smFontSize,
								color: theme.colors.primaryColor,
							}}
						/>
					}>
					{options.map(opt => {
						return (
							<Option
								value={opt[customKey]}
								key={"key" + "_" + opt[customKey] + "_" + "order"}>
								{opt[valueKey]}
							</Option>
						);
					})}
				</Select>
			</div>
		</>
	);
};
