import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { showSocialHousing } from "../../../Utils/Functions";
import { useFilters } from "../Filters.hook";

const FRAGMENT_SOCIALHOUSING_OPTIONS = new FragmentDefiner(
	"Filter",
	`
		id
		name
	`
);

export const FRAGMENT_SOCIALHOUSING = new FragmentDefiner(
	"Filters",
	`
		socialHousing {
			${FRAGMENT_SOCIALHOUSING_OPTIONS.query()}
		}
	`
);

export const useSocialHousing = props => {
	const { filters, changeFilters } = useFilters();
	const {
		labeled = false,
		selectedValue = filters?.socialHousing,
		inputType = "select",
		currentFilters = filters,
		filterChanged = changeFilters,
	} = props;
	const { loading, data, error } = useReadFragment(
		FRAGMENT_SOCIALHOUSING_OPTIONS,
		"socialHousing"
	);

	const handleChange = () => {
		let res = !selectedValue ? { value: !selectedValue, text: data.name } : null;
		filterChanged({ socialHousing: res });
	};

	const show = showSocialHousing(
		currentFilters?.property_type_id,
		currentFilters?.operation_type_id
	);

	return {
		loading: loading,
		error: error,
		show, // @todo: handle show property for this filter
		data: {
			text: data?.name,
			checked: selectedValue,
			handleClick: handleChange,
		},
	};
};
