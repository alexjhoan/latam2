import { Col, Collapse, Radio, Row, Select, Skeleton, Space, Tooltip, Typography } from "antd";

import { DownOutlined } from "@ant-design/icons";
import { usePublicationDate } from "./PublicationDate.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

export const PublicationDate = ({ collapsable = false, ...props }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		show,
		error,
		loading,
		labeled,
		data: { options, valueKey, customKey, onChange, value },
	} = usePublicationDate(props);

	if (!show) return null;
	if (error) return <div>{t("error")}</div>;

	const handleRadio = e => {
		let val = e.target.value;
		const res = options.filter(o => o[customKey] == val);
		onChange(res[0]);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Radio.Group onChange={handleRadio} value={value}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				<Col span={24}>
					<Radio value={null}>{t("Indiferente")}</Radio>
				</Col>
				{options?.map(o => (
					<Col span={24} key={`key_${o[customKey]}_publicationDate`}>
						<Radio value={o[customKey]}>{t(o[valueKey])}</Radio>
					</Col>
				))}
			</Row>
		</Radio.Group>
	);

	const label = "Fecha de publicación";

	return (
		<div className="filter publicationDate-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={value ? "true" : "false"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={value !== null && value >= 0 && extraIcon()}
						disabled={loading}
						header={t(label)}
						key="true">
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title disabled={loading} level={4}>
							{t(label)}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
