import { Label } from "../../Inputs/Label/app";
import { InputAutocomplete } from "../../Inputs/app";
import { useLocation } from "./Location.hook";
import { FilterComponentType } from "../Filters.hook";
import { Text, View } from "react-native";
import React from "react";

export const Location = (props: FilterComponentType) => {
	const { data, error, show, label, labeled } = useLocation(props);

	if (!show) return null;
	if (error) return <Text>{JSON.stringify(error)}</Text>;

	return (
		<>
			<View>
				<Label labelContent={label} showLabel={labeled}>
					<InputAutocomplete {...data} openOnEmpty={true} />
				</Label>
			</View>
		</>
	);
};
