import { Checkbox, Col, Collapse, Row, Skeleton, Space, Tooltip, Typography } from "antd";
import { FloorProps, useFloors } from "./Floors.hook";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

export const Floors = ({ labeled = false, collapsable = false, ...props }: FloorProps) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const { floors, show } = useFloors(props);

	if (!show) return null;

	const handleChecked = checked => {
		floors.set(checked);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = floors.loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Checkbox.Group value={floors.value} onChange={handleChecked}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{floors.options?.map(o => (
					<Col span={24} key={`key_${o["value"]}_floors`}>
						<Checkbox value={o["value"]}>{t(o["text"])}</Checkbox>
					</Col>
				))}
			</Row>
		</Checkbox.Group>
	);

	const label = "Pisos";

	return (
		<div className="filter floors-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={floors?.value?.length >= 1 ? "true" : "false"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={floors?.value?.length >= 1 && extraIcon()}
						disabled={floors.loading}
						header={t(label)}
						key="true">
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title disabled={floors.loading} level={4}>
							{t(label)}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
