import "./styles.less";

import {
	Button,
	Checkbox,
	Col,
	Collapse,
	Input,
	InputNumber,
	Radio,
	Row,
	Skeleton,
	Space,
	Tooltip,
	Typography,
} from "antd";
import { CheckOutlined, DownOutlined } from "@ant-design/icons";
import { PriceFilterProps, usePrice } from "./Price.hook";
import { formatMoney, parserMoney } from "../../../Utils/Functions";

import { RadioChangeEvent } from "antd/lib/radio";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;
const { Title } = Typography;

export const Price = ({ labeled = false, collapsable = false, ...props }: PriceFilterProps) => {
	const { theme } = useTheme();
	const { t } = useTranslation();

	const { commonEx, maxPrice, minPrice, currency, showCommonExpenses, saveFilter } = usePrice(
		props
	);

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const currencyFilter = currency.loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Radio.Group
			value={currency.value}
			defaultValue={1}
			onChange={(e: RadioChangeEvent) => {
				const id = e.target.value;
				const val = currency.options.find(o => o["id"] == id);
				currency.set({ text: val.nombre, value: val.id });
			}}
			disabled={currency.loading}>
			<Row gutter={[0, theme.spacing.lgSpacing]}>
				{currency.options?.map(opt => (
					<Col span={24} key={`currency-${opt.id}`}>
						<Radio value={opt.id}>{t(opt.nombreCompleto)}</Radio>
					</Col>
				))}
			</Row>
		</Radio.Group>
	);

	const range = (
		<Input.Group>
			<InputNumber
				className="secondary"
				disabled={currency.loading}
				placeholder={t("Min") + (currency.text ? " " + currency.text : "")}
				value={minPrice.value}
				onChange={e => minPrice.set(e)}
				formatter={value => formatMoney(value)}
				parser={value => parserMoney(value)}
				pattern="\d*"
				onPressEnter={saveFilter}
			/>
			<Input className="input-separate" placeholder="-" disabled />
			<InputNumber
				className="secondary"
				disabled={currency.loading}
				placeholder={t("Max") + (currency.text ? " " + currency.text : "")}
				value={maxPrice.value}
				onChange={maxPrice.set}
				formatter={value => formatMoney(value)}
				parser={value => parserMoney(value)}
				pattern="\d*"
				onPressEnter={saveFilter}
			/>
			<Button
				disabled={currency.loading}
				ghost
				icon={<CheckOutlined />}
				onClick={saveFilter}
			/>
		</Input.Group>
	);

	const commonExpensesFilter = showCommonExpenses ? (
		<Checkbox checked={commonEx.value} onChange={e => commonEx.set(e.target.checked)}>
			{t("Gastos Comunes")}
		</Checkbox>
	) : null;

	const filter = (
		<Space direction={"vertical"} size="small">
			{currencyFilter}
			{range}
			{commonExpensesFilter}
		</Space>
	);

	const label = t("Rango de Precios");

	return (
		<div className="filter price-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={minPrice.value || maxPrice.value ? "false" : "true"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={(minPrice.value > 0 || maxPrice.value > 0) && extraIcon()}
						header={label}
						key={"true"}
						disabled={currency.loading}>
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Title level={4} disabled={currency.loading}>
							{label}
						</Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
