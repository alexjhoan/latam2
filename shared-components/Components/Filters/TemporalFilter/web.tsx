import "./styles.less";

import { Button, Collapse, Tooltip, Typography } from "antd";
import { CalendarFilled, ContactsFilled, DownOutlined } from "@ant-design/icons";

import { DateRange } from "./DateRange/web";
import React from "react";
import { Seasons } from "./Seasons/web";
import { useTemporalFilter } from "./TemporalFilter.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;
const { Title } = Typography;

export const TemporalFilter = ({ filterChanged, labeled = false, collapsable = false }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		isTemporal,
		filters: { dateTo, dateFrom, season },
		setShowSeasonInput,
		showSeasonInput,
	} = useTemporalFilter();

	if (!isTemporal) return null;

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = (
		<>
			{showSeasonInput ? (
				<Seasons filterChanged={filterChanged} />
			) : (
				<DateRange filterChanged={filterChanged} />
			)}
			<Button
				className="btn-search-seasons"
				type="text"
				onClick={() => setShowSeasonInput(!showSeasonInput)}
				icon={showSeasonInput ? <CalendarFilled /> : <ContactsFilled />}>
				<Typography.Text>
					{showSeasonInput ? t("Buscar por Calendario") : t("Buscar por Temporada")}
				</Typography.Text>
			</Button>
			<style jsx global>{`
				.home-filters .temporal-filter {
					border-right: 1px solid ${theme.colors.borderColor};
				}

				.home-filters .temporal-filter .ant-btn {
					background: ${theme.colors.backgroundModalColor};
					padding: 0px ${theme.spacing.smSpacing}px;
					border-radius: ${theme.spacing.smSpacing}px;
					font-size: ${theme.spacing.mdSpacing + 1}px;
				}

				.home-filters .temporal-filter .ant-select-selector,
				.ant-picker {
					border-radius: ${theme.spacing.smSpacing}px 0 0 ${theme.spacing.smSpacing}px;
				}

				.home-filters .temporal-filter .ant-btn span {
					color: ${theme.colors.backgroundColorAternative};
				}

				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.home-filters .temporal-filter .ant-btn {
						background: transparent;
					}

					.home-filters .temporal-filter .ant-select-selector,
					.ant-picker {
						border-radius: ${theme.spacing.smSpacing}px;
					}
				}
			`}</style>
		</>
	);

	return (
		<div className="filter temporal-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					expandIconPosition="right"
					defaultActiveKey={dateFrom || dateTo || season ? "true" : "false"}
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={(dateFrom || dateTo || season) && extraIcon()}
						header={t("Temporada") + " / " + t("Fechas")}
						key={"true"}>
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Title level={4}>{showSeasonInput ? t("Temporada") : t("Fechas")}</Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
