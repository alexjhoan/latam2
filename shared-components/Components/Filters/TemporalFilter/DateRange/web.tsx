import "moment/locale/es";

import { DatePicker, Typography } from "antd";
import { DateRangeProps, useDateRange } from "./DateRange.hook";
import dayjs, { Dayjs } from "dayjs";

import { DateRangePickerMobile } from "./CustomCalendar/web";
import { RangePickerDateProps } from "antd/lib/date-picker/generatePicker";
import customParseFormat from "dayjs/plugin/customParseFormat";
import locale from "antd/lib/date-picker/locale/es_ES";
import moment from "moment";
import useBreakpoint from "antd/lib/grid/hooks/useBreakpoint";
import { useState } from "react";
import { useTranslation } from "react-i18next";

const { RangePicker } = DatePicker;

moment.locale("es");

export const DateRange = ({ filterChanged }: DateRangeProps) => {
	const { dateFrom, dateTo } = useDateRange();
	const { t } = useTranslation();
	dayjs.extend(customParseFormat);

	const screen = useBreakpoint();
	const [open, setOpen] = useState<boolean>(false);

	const vars: RangePickerDateProps<Dayjs> = {
		disabledDate: d => d.isBefore(dayjs(new Date()), "date"),
		defaultValue: [dayjs(new Date()), null],
		value: [
			dateFrom ? dayjs(dateFrom, "YYYY-MM-DD") : null,
			dateTo ? dayjs(dateTo, "YYYY-MM-DD") : null,
		],
		placeholder: [t("Llegada"), t("Salida")],
		format: "YYYY-MM-DD",
		onChange: (date, dateString) => {
			filterChanged({
				dateFrom: date && date[0] ? { value: dateString[0], text: dateString[0] } : null,
				dateTo: date && date[1] ? { value: dateString[1], text: dateString[1] } : null,
				season: null,
			});
		},
		onOpenChange: e => setOpen(e),
		allowClear: true,
	};

	return (
		<>
			<RangePicker
				locale={locale}
				className="secondary"
				open={open && screen.lg}
				dropdownClassName="range-picker-custom secondary"
				{...vars}
			/>
			<DateRangePickerMobile
				open={open && !screen.lg}
				{...vars}
				clearIcon={
					<Typography.Text underline style={{ fontSize: "15px" }}>
						{t("Borrar")}
					</Typography.Text>
				}
			/>
		</>
	);
};
