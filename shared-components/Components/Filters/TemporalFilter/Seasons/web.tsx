import "./styles.less";

import { SeasonFilterProps, useSeasons } from "./Seasons.hook";

import { Select } from "antd";
import { useTranslation } from "react-i18next";

const { Option } = Select;

export const Seasons = ({ filterChanged }: SeasonFilterProps) => {
	const {
		initialValue,
		options: { loading, options },
	} = useSeasons();

	const { t } = useTranslation();

	const seasonChange = (value, option) => {
		filterChanged({
			season: typeof option != "undefined" ? { text: option.children, value } : null,
			dateFrom: null,
			dateTo: null,
		});
	};

	return (
		<Select
			className="select-seasons secondary"
			allowClear
			disabled={loading}
			value={initialValue}
			onChange={seasonChange}
			placeholder={t("Temporada")}>
			{options?.map((opt, i) => {
				return (
					<Option key={`${opt.value}-${i}`} value={opt.value}>
						{opt.text}
					</Option>
				);
			})}
		</Select>
	);
};
