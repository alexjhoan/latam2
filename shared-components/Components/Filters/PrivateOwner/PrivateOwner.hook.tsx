import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { showPrivateOwner } from "../../../Utils/Functions";
import { useFilters } from "../Filters.hook";
import { useState } from "react";

const FRAGMENT_PRIVATEOWNER_OPTIONS = new FragmentDefiner(
	"Filter",
	`
		id
		name
	`
);

export const FRAGMENT_PRIVATEOWNER = new FragmentDefiner(
	"Filters",
	`
		privateOwner {
			${FRAGMENT_PRIVATEOWNER_OPTIONS.query()}
		}
	`
);
export interface PrivateOwnerProps {
	filterChanged: ({}: any) => void;
	labeled?: boolean;
}
export const usePrivateOwner = ({ filterChanged, labeled }: PrivateOwnerProps) => {
	const { filters, filtersTags } = useFilters();
	const [selected, setSelected] = useState(filters?.privateOwner);

	const { loading, data, error } = useReadFragment(FRAGMENT_PRIVATEOWNER_OPTIONS, "privateOwner");

	const handleChange = value => {
		setSelected(value);
		let res = { value: value, text: value ? "Dueño" : "Inmobiliaria" };
		filterChanged({ privateOwner: res });
	};

	const show = showPrivateOwner(filters?.operation_type_id);

	return {
		loading: loading,
		error: error,
		show,
		data: {
			text: data?.name,
			checked: selected,
			handleClick: handleChange,
		},
	};
};
