import { Checkbox, Col, Collapse, Row, Select, Skeleton, Space, Tooltip, Typography } from "antd";
import { FacilitiesProps, useFacilities } from "./Facilities.hook";

import { DownOutlined } from "@ant-design/icons";
import React from "react";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Panel } = Collapse;

export const Facilities = ({
	collapsable = false,
	labeled = false,
	inputType = "select",
	...props
}: FacilitiesProps) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const { facilities, show } = useFacilities(props);

	if (!show) return null;

	const handleChecked = (checked = []) => {
		const res = facilities.options.filter(o => checked.includes(o["id"]));
		facilities.set(res);
	};

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filterChackbox = facilities.loading ? (
		<Space style={{ width: "100%" }} size={0} direction={"vertical"}>
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
			<Skeleton title={false} active paragraph={{ rows: 1, width: "100%" }} />
		</Space>
	) : (
		<Checkbox.Group value={facilities.selected} onChange={handleChecked}>
			<Row>
				{facilities.options?.map(o => (
					<Col span={24} key={`key_${o["id"]}_facilities`}>
						<Checkbox value={o["id"]}>{t(o["nombre"])}</Checkbox>
					</Col>
				))}
			</Row>
		</Checkbox.Group>
	);

	const filterSelect = (
		<Select
			dropdownClassName="secondary"
			disabled={facilities.loading}
			value={facilities.loading ? undefined : facilities.selected}
			className="secondary"
			allowClear
			onChange={handleChecked}
			mode={"multiple"}
			notFoundContent={t("No se encontraron resultados")}
			optionFilterProp={"children"}
			placeholder={t("Seleccione los amenities")}
			style={{ width: "100%" }}>
			{facilities.options?.map(o => (
				<Select.Option key={`key_${o["id"]}_facilities`} value={o["id"]}>
					{t(o["nombre"])}
				</Select.Option>
			))}
		</Select>
	);

	const filter = inputType == "select" ? filterSelect : filterChackbox;
	const label = "Amenities";

	return (
		<div className="filter facilities-filter">
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					defaultActiveKey={facilities.value?.length >= 1 ? "true" : "false"}
					expandIconPosition="right"
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={facilities.value?.length >= 1 && extraIcon()}
						header={t(label)}
						key="true"
						disabled={facilities.loading}>
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Typography.Title level={4} disabled={facilities.loading}>
							{label}
						</Typography.Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
