import "./styles.less";

import { Collapse, Input, Tooltip, Typography } from "antd";
import React, { useEffect, useState } from "react";

import { DownOutlined } from "@ant-design/icons";
import { useGuests } from "./Guests.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Search } = Input;
const { Panel } = Collapse;
const { Title } = Typography;

export const Guests = ({ collapsable = false, ...props }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const {
		show,
		error,
		loading,
		labeled,
		data: { defaultValue, search },
	} = useGuests(props);

	const [guests, setGuests] = useState(null);
	useEffect(() => setGuests(defaultValue), [defaultValue]);

	if (!show) return null;
	if (error) return <div>{t("error")}</div>;

	const textTooltip = "Filtro Activo";

	const extraIcon = () => (
		<Tooltip
			arrowPointAtCenter
			placement="topLeft"
			overlayInnerStyle={{ fontSize: 12, padding: 8 }}
			color={theme.colors.secondaryHoverColor}
			title={textTooltip}>
			<span className="active-filter"></span>
		</Tooltip>
	);

	const filter = (
		<Search
			className="secondary"
			disabled={loading}
			enterButton
			type={"number"}
			value={guests}
			placeholder={t("Huespedes")}
			onChange={e => setGuests(e.target.value)}
			onSearch={() => search(guests)}
			onPressEnter={() => search(guests)}
		/>
	);

	const label = "Huespedes";

	return (
		<div className={"filter guests-filter"}>
			{collapsable ? (
				<Collapse
					ghost
					bordered={false}
					expandIconPosition="right"
					defaultActiveKey={defaultValue >= 1 ? "true" : "false"}
					expandIcon={({ isActive }) => <DownOutlined rotate={isActive ? 180 : 0} />}>
					<Panel
						extra={defaultValue >= 1 && extraIcon()}
						header={t(label)}
						key={"true"}
						disabled={loading}>
						{filter}
					</Panel>
				</Collapse>
			) : (
				<>
					{labeled && (
						<Title disabled={loading} level={4}>
							{t(label)}
						</Title>
					)}
					{filter}
				</>
			)}
		</div>
	);
};
