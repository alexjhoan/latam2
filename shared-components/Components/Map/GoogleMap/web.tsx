import React, { useRef, useState } from "react";
import GoogleMapReact from "google-map-react";
import Measure from "react-measure";

export function GoogleMap({
	defaultZoom = 18,
	height = null,
	width = null,
	streetView = false,
	center,
	children = null,
	options = {},
}) {
	const MAP = useRef(null);
	const MAPS = useRef(null);
	const mapDOMElement = useRef(null);
	const [measure, setMeasure] = useState(null);

	const handleApiLoaded = (map, maps) => {
		MAP.current = map;
		MAPS.current = maps;

		if (streetView) {
			new maps.StreetViewPanorama(mapDOMElement.current.googleMapDom_ as HTMLElement, {
				position: { lat: center.latitude, lng: center.longitude },
				pov: { heading: 165, pitch: 0 },
				zoom: 1,
			});
		}
	};

	let googleMapReactOptions = {};
	if (options && options.marker_size) {
		googleMapReactOptions = {
			...googleMapReactOptions,
			distanceToMouse: (markerPos, mousePos, markerProps) => {
				const x = markerPos.x + options.marker_size / 2;
				const y = markerPos.y + options.marker_size / 2;
				const distanceKoef = 1; //markerProps.text !== 'A' ? 1.5 : 1;
				return (
					distanceKoef *
					Math.sqrt(
						(x - mousePos.x) * (x - mousePos.x) + (y - mousePos.y) * (y - mousePos.y)
					)
				);
			},
			hoverDistance: options.marker_size + 0,
		};
	}

	return (
		<Measure bounds onResize={m => m.entry && setMeasure(m)}>
			{({ measureRef }) => (
				<div ref={measureRef} style={{ width: "100%" }}>
					{measure && measure.entry && measure.entry.height && measure.entry.width && (
						<div
							style={{
								height: height ? height : measure.entry.height,
								width: width ? width : measure.entry.width,
								position: "relative",
							}}>
							<GoogleMapReact
								ref={mapDOMElement}
								bootstrapURLKeys={{
									key: "AIzaSyDBA9FLlSyCGsbvPX-IGZm5BdAFnM04zUw",
								}}
								defaultCenter={{ lng: center.longitude, lat: center.latitude }}
								defaultZoom={defaultZoom}
								options={{
									clickableIcons: false,
									draggable: true,
									zoomControl: true,
									scrollwheel: true,
									disableDoubleClickZoom: false,
								}}
								{...googleMapReactOptions}
								yesIWantToUseGoogleMapApiInternals
								onGoogleApiLoaded={x => handleApiLoaded(x.map, x.maps)}>
								{children}
							</GoogleMapReact>
						</div>
					)}
				</div>
			)}
		</Measure>
	);
}
