import React, { useEffect, useRef } from "react";
import { PriceTag } from "../../../Property/PriceTag/web";
import { useSeenStatus } from "../../../Property/SeenStatus/SeenStatus.hook";
import { useTheme } from "../../../../Styles/ThemeHook";

const K_LISTING_MARKER_SIZE = 10;

const ListingMarker = props => {
	const {
		key,
		id,
		text,
		lat,
		lng,
		$hover = false,
		active,
		zoomPrecio,
		updateInfoWindowPosition,
		onClick,
	} = props;

	const { result: seen } = useSeenStatus({ id });
	const ref = useRef(null);
	const { theme } = useTheme();

	const dotColor = seen ? "#9b9b9b" : theme.colors.primaryColor;

	useEffect(() => {
		if (active) updateInfoWindowPosition(ref.current?.getBoundingClientRect());
	}, [active]);

	const handleClick = e => {
		e.stopPropagation();
		if (typeof onClick == "function") onClick(id, props);
	};

	return (
		<>
			<div className={"map-marker"} ref={ref} onClick={handleClick}>
				{zoomPrecio ? (
					<div className={"map-marker-price"}>
						<div className={"content"}>
							<PriceTag
								id={id}
								showExpenses={false}
								threeDigitsFormat={true}
								showFromText={false}
							/>
						</div>
						<div className={"arrow"} />
					</div>
				) : (
					<div className={"map-marker-dot"} />
				)}
			</div>
			<style jsx>
				{`
					.map-marker-dot {
						height: ${theme.spacing.mdSpacing}px;
						width: ${theme.spacing.mdSpacing}px;
						background-color: ${$hover || active ? theme.colors.secondaryColor : dotColor};
						border-radius: 50%;
						border: 2px solid white;
						z-index: ${$hover || active ? 3 : 2};
						cursor: pointer;
						box-shadow: none;
						box-shadow:${
							$hover
								? "0 0 0 " +
								  theme.spacing.smSpacing +
								  "px " +
								  theme.colors.primaryHoverColor
								: "none"
						}
						transition: 0.2s;
						transform: translate(-50%, -50%);
					}
					.map-marker-price {
						position:relative;
						z-index: ${$hover || active ? 3 : 2};
						width: max-content;
						transform: translate(-50% ,${theme.spacing.smSpacing}px);
						display: flex;
						flex-direction:column;
						align-items: center;
					}
					.map-marker-price .content{
						padding: ${theme.spacing.xsSpacing / 2}px ${theme.spacing.smSpacing}px;
						border-radius: ${theme.spacing.smSpacing}px;
						box-shadow: ${"0 0 0px 2px" + theme.colors.backgroundColor};
						background: ${$hover || active ? theme.colors.secondaryColor : dotColor};
					}
					.map-marker-price .content :global(.price) {
						font-size: 12px;
						color:${theme.colors.backgroundColor};
					}
					.map-marker-price .arrow{
						width: 0;
						height: 0;
						border-width: ${theme.spacing.smSpacing}px;
						border-color: transparent;
						border-style: solid;
						border-top-color: ${$hover || active ? theme.colors.secondaryColor : dotColor};
					}
					.map-marker-price:hover{
						z-index: 3;
					}
					.map-marker-price:hover .content{
						background:${theme.colors.secondaryColor};
					}
					.map-marker-price:hover .arrow{
						border-top-color: ${theme.colors.secondaryColor};
					}
				`}
			</style>
		</>
	);
};

export { ListingMarker, K_LISTING_MARKER_SIZE };
