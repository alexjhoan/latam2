import { FRAGMENT_PROPERTY_CARD } from "../../../Property/PropertyCard/PropertyCard.hook";
import { useLazyQuery } from "@apollo/client";
import { gql } from "@apollo/client";
import { useFilters } from "../../../Filters/Filters.hook";

const QUERY_SEARCH_RESULTS_CARDS = gql`
    query InfoWindowListingQuery($rows: Int!, $params: SearchParamsInput!, $page: Int) {
        properties: searchListing(params: $params, first: $rows, page: $page) {
            data {
               __typename
               id
                ${FRAGMENT_PROPERTY_CARD.query()}
            }
             paginatorInfo {
                lastPage
                firstItem
                lastItem
                total
             }
        }
    }
`;

const useListingInfoWindow = property_id => {
	const { filters } = useFilters();

	const [doLazyQuery, { data, loading, error }] = useLazyQuery(
		QUERY_SEARCH_RESULTS_CARDS,
		{
			variables: {
				rows: 1,
				params: { ...filters, property_id: property_id },
				page: 1,
			},
		}
	);

	return {
		data:
			data && data.properties.data.length > 0 ? data.properties.data[0] : null,
		loading,
		error,
		doLazyQuery,
	};
};

export { useListingInfoWindow };
