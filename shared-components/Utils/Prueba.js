import React from "react";
import { useContext, useState } from "react";
import { useApolloClient } from "@apollo/client";
import { DummyDispatchContext, DummyStateContext } from "../Contexts/Dummy/context";
import { FiltersStateContext } from "../Contexts/Filters/context";


const usePrueba = () => {

   const [state, setState] = useState(0);

   return {
      state,
      setState
   }

};


export const ComponenteC = () => {
   const state = useContext(DummyStateContext);

   return (
      <>
         <h2>Componente C</h2>
         <span>YYYYY Componente C: {JSON.stringify(state)}</span><br /><br />
      </>);
};


export const ComponenteD = () => {
   const dispatch = useContext(DummyDispatchContext);

   return (
      <>
         <h2>Componente D</h2>
         <button onClick={() => dispatch({ type: "increment" })}>INCREMENT</button><br />
         <button onClick={() => dispatch({ type: "decrement" })}>DECREMENT</button>

      </>);
};

export const ComponenteA = () => {
   const { state, setState } = usePrueba();
   const filters = useContext(FiltersStateContext);

   return (
      <><span>YYYYYYYY</span>
         <h1>ComponenteAs</h1>
         <span>Filters state: {JSON.stringify(filters)}</span><br /><br />
         <span>state: {state}</span><br /><br />
         <button onClick={() => { setState(state + 1); setState(state + 1) }}>Incrementar 2</button><br /><br />
      </>
   )
}




export const ComponenteB = () => {
   const { state, setState } = usePrueba();
   const client = useApolloClient()


   return (
      <>
         <h1>ComponenteB</h1>
         <button onClick={() => {
            client.writeQuery({
               query: QUERY_LOCAL_GET_FILTERS,
               data: { filters: { map_polygon: { text: "Prueba", value: [] } } },
            });
            return;
            mutateFilters({
               map_polygon: {
                  text: "POLYGON",
                  value: [{ longitude: -56.1494064, latitude: -34.9030204 }, { longitude: -56.1458659, latitude: -34.9028796 }, { longitude: -56.1446428, latitude: -34.9027564 }, { longitude: -56.1437416, latitude: -34.902598 }, { longitude: -56.1403513, latitude: -34.9017797 }, { longitude: -56.1385489, latitude: -34.9011901 }, { longitude: -56.1349225, latitude: -34.9111854 }, { longitude: -56.136446, latitude: -34.9112734 }, { longitude: -56.1372614, latitude: -34.9117485 }, { longitude: -56.1380982, latitude: -34.91203 }, { longitude: -56.1388063, latitude: -34.9117837 }, { longitude: -56.1392999, latitude: -34.9111326 }, { longitude: -56.1399007, latitude: -34.9102528 }, { longitude: -56.1403728, latitude: -34.9099889 }, { longitude: -56.1408877, latitude: -34.9098657 }, { longitude: -56.1414242, latitude: -34.9099009 }, { longitude: -56.1421108, latitude: -34.9096545 }, { longitude: -56.1427547, latitude: -34.9095313 }, { longitude: -56.1432695, latitude: -34.9096721 }, { longitude: -56.1440206, latitude: -34.9103232 }, { longitude: -56.1462951, latitude: -34.9123291 }, { longitude: -56.1483121, latitude: -34.9142647 }, { longitude: -56.1487842, latitude: -34.9151972 }, { longitude: -56.1489773, latitude: -34.9162529 }, { longitude: -56.1489344, latitude: -34.917379 }, { longitude: -56.1486769, latitude: -34.9184522 }, { longitude: -56.1559939, latitude: -34.9166224 }, { longitude: -56.1563695, latitude: -34.9142998 }, { longitude: -56.162796, latitude: -34.9148453 }, { longitude: -56.1641479, latitude: -34.9021757 }, { longitude: -56.1614227, latitude: -34.9028796 }, { longitude: -56.1591268, latitude: -34.9031084 }, { longitude: -56.1561012, latitude: -34.9031612 }, { longitude: -56.1528182, latitude: -34.903126 }, { longitude: -56.1494064, latitude: -34.9030204 }]
               }
            })
         }
         }>CHANGE FILTERS STATE </button><br />
         <span>state: {state}</span><br />
         <button onClick={() => setState(state + 1)}>Incrementar 1</button><br />
         <button onClick={() => { setState(state + 1); setState(state + 1) }}>Incrementar 2</button>
      </>
   )
}





