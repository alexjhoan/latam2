import React, { useState, useEffect } from "react";
import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { FRAGMENT_LISTINGTYPETAG } from "../../../Components/Property/ListingTypeTag/ListingType.hook";
import { FRAGMENT_PROPERTY_COVER_TAG } from "./PropertyCoverTag/hook";
import { PropComponentMode } from "../../../Components/Property/PropertyInterfaces";

export const FRAGMENT_PROPERTY_COVER = new FragmentDefiner(
	"Property",
	`
    id
    img
    image_count
    youtube
    images {
        image
    }project{
        id
        images{
            image
            tag
        }
    }
`
).uses(FRAGMENT_LISTINGTYPETAG, FRAGMENT_PROPERTY_COVER_TAG);

export interface ImageProps {
	id: string;
	mode: PropComponentMode;
	onTabClick: (tab: any) => void;
}

export const usePropertyCover = (id: string, mode: PropComponentMode) => {
	const { loading, data, error } = useReadFragment(FRAGMENT_PROPERTY_COVER, id);
	const [dataImg, setDataImg] = useState(null);

	useEffect(() => {
		if (data) {
			if ((data.project[0] && mode == "auto") || mode == "project") {
				let imgs = data.project[0].images?.filter(
					image => image.tag != "construction_advances"
				);
				setDataImg({
					img: imgs[0]?.image,
					images: imgs,
				});
			} else {
				setDataImg({
					img: data.img.length > 0 ? data.img : data.images[0]?.image,
					images: data.images,
				});
			}
		}
	}, [data]);

	return {
		loading,
		data: dataImg,
		error,
	};
};
