import "./styles.less";

import { Col, Row, Skeleton, Space } from "antd";
import { ImageProps, usePropertyCover } from "./hook";
import React, { FC } from "react";

import { Favorite } from "../../../Components/Property/Favorite/web";
import { ImagenOptimizada } from "../../../Components/Image/web";
import { ListingTypeTag } from "../../../Components/Property/ListingTypeTag/web";
import { PropertyCoverTags } from "./PropertyCoverTag/web";
import { SocialShare } from "../../../Components/Property/SocialShare/web";
import { useTheme } from "../../../Styles/ThemeHook";

export const PropertyCover: FC<ImageProps> = ({ id, mode = "auto", onTabClick }) => {
	const { loading, error, data } = usePropertyCover(id, mode);
	const { theme } = useTheme();

	if (error) console.log(error.message);
	if (!loading && !data) return null;

	const fullWidth = data?.images?.length <= 2;

	return (
		<div className={"property_details_cover"}>
			<div
				className="cover-gradient"
				onClick={loading ? null : () => onTabClick("photos")}></div>
			<Row className={"top-buttons"}>
				<Col flex={1}>
					<ListingTypeTag id={id} />
				</Col>
				<Col>
					<Space size="middle">
						<Favorite id={id} />
						<SocialShare id={id} mode={mode} />
					</Space>
				</Col>
			</Row>

			<Row className={"bottom-tags"}>
				<Col>
					<PropertyCoverTags onClick={tab => onTabClick(tab)} id={id} />
				</Col>
			</Row>

			<Row
				gutter={theme.spacing.smSpacing}
				className={"property-cover-gallery"}
				onClick={loading ? null : () => onTabClick("photos")}>
				<Col span={24} lg={fullWidth ? 24 : 18}>
					{loading ? <Skeleton.Image /> : <ImagenOptimizada src={data.img} />}
				</Col>
				{!fullWidth && (
					<Col span={0} lg={6} className="left">
						<Row gutter={[0, theme.spacing.smSpacing]}>
							<Col span={24}>
								{loading ? (
									<Skeleton.Image />
								) : (
									<ImagenOptimizada src={data.images[1].image} />
								)}
							</Col>
							<Col span={24}>
								{loading ? (
									<Skeleton.Image />
								) : (
									<ImagenOptimizada src={data.images[2].image} />
								)}
							</Col>
						</Row>
					</Col>
				)}
			</Row>

			<style jsx global>{`
				@media screen and (min-width: ${theme.breakPoints.lg}) {
					.property_details_cover {
						border-radius: ${theme.spacing.smSpacing}px;
					}
				}
				.property_details_cover .top-buttons,
				.property_details_cover .bottom-tags {
					padding: ${theme.spacing.mdSpacing}px;
				}
				.property_details_cover .property-cover-gallery {
					width: calc(100% + ${theme.spacing.smSpacing}px);
				}
				.property_details_cover .property-cover-gallery .left .ant-col {
					height: calc(50% + ${theme.spacing.smSpacing}px);
				}
			`}</style>
		</div>
	);
};
