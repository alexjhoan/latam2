import { FragmentDefiner, useReadFragment } from "../../../GlobalHooks/useReadFragment";
import { FRAGMENT_LISTINGTYPETAG } from "../../../Components/Property/ListingTypeTag/ListingType.hook";
import { FRAGMENT_IMAGE } from "../../../Components/Property/LazyImageGallery/LazyImageGallery.hook";
import { FRAGMENT_PROPERTY_MODAL_VIDEO } from "./PropertyModalVideo/PropertyModalVideo.hook";
import { FRAGMENT_PROPERTY_MODAL_FLOOR_PLANS } from "./PropertyModalFloorPlans/PropertyModalFloorPlans.hook";
import { FRAGMENT_MAP } from "../../../Components/Property/PropertyMap/hook";
import { useState, useEffect } from "react";

export const FRAGMENT_PROPERTY_MODAL = new FragmentDefiner(
	"Property",
	`
    id
    img
    image_count
	youtube
	latitude
    longitude
    project {
        id
	}
`
).uses(
	FRAGMENT_LISTINGTYPETAG,
	FRAGMENT_IMAGE,
	FRAGMENT_PROPERTY_MODAL_VIDEO,
	FRAGMENT_PROPERTY_MODAL_FLOOR_PLANS,
	FRAGMENT_MAP
);

export function usePropertyModal({ id, mode }) {
	const [isProject, setIsProject] = useState(false);
	const { loading, data, error } = useReadFragment(FRAGMENT_PROPERTY_MODAL, id);

	useEffect(() => {
		if (data) {
			if ((data.project[0] && mode == "auto") || mode == "project") {
				setIsProject(true);
			} else {
				setIsProject(false);
			}
		}
	}, [data]);

	return {
		loading,
		data,
		error,
		isProject,
	};
}
