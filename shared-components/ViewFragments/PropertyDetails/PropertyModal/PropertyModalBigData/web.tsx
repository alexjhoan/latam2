import React from "react";
import { BigDataProperties } from "../../../../Components/Property/BigDataProperties/web";

export function PropertyModalBigData({ id }) {
	return <BigDataProperties id={id} scrollable />;
}
