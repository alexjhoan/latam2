import "./styles.less";

import { Col, Row, Skeleton, Typography } from "antd";

//import { ChartLine } from "../../../../Components/Charts/web";
import React from "react";
import { useBigDataSingleProp } from "../../../../Components/BigData/SingleProp/hook";
import { usePropertySimilarsCharts } from "./hook";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";
import { LineChart } from "../../../../Components/Charts/LineChart/web";

const { Title } = Typography;

export function PropertySimilarsCharts({ id }) {
	const { error, loading, chartPrice, chartPriceM2 } = usePropertySimilarsCharts({
		property_id: id,
	});
	const { bigData, error: errorBigData, loading: loadingBigData, show } = useBigDataSingleProp({
		property_id: id,
	});
	const { t } = useTranslation();
	const { theme } = useTheme();

	if (error) return null;

	return (
		<>
			<Row gutter={[theme.spacing.lgSpacing, theme.spacing.lgSpacing]}>
				<Col xs={24} sm={24} md={12}>
					<Title level={4}>{t("Precio por m² para propiedades similares")}</Title>
					{loading || loadingBigData ? (
						<Skeleton active />
					) : (
						<div className="similars-chart">
							<LineChart 
								data={chartPriceM2}
								activeColor={theme.colors.primaryColor}
								actualPropId={id}
								lineColor={theme.colors.secondaryColor}
								lineWidth={4}
								average={bigData?.avg_price_m2}
								title={t("Precio por m²")}
								currency="U$S"
							/>
						</div>

						/*
						<ChartLine
							className={"graphic_avg_price_m2"}
							labels={chartPriceM2.map(o => o.label)}
							datasets={[
								{
									name: t("Precio por m²"),
									values: chartPriceM2.map(o => o.value),
									colors: chartPriceM2.map(o =>
										o.id == id
											? theme.colors.primaryColor
											: theme.colors.secondaryColor
									),
								},
							]}
							stroke={{ width: 4, color: theme.colors.secondaryColor }}
							height={250}
							trendline={{ label: "Promedio", value: bigData?.avg_price_m2 }}
						/>
						*/
					)}
				</Col>
				<Col xs={24} sm={24} md={12}>
					<Title level={4}>{t("Precio para propiedades similares")}</Title>
					{loading || loadingBigData ? (
						<Skeleton active />
					) : (
						<div className="similars-chart">
							<LineChart 
								data={chartPrice}
								lineColor={theme.colors.secondaryColor}
								activeColor={theme.colors.primaryColor}
								actualPropId={id}
								lineWidth={4}
								average={bigData?.avg_price}
								title={t("Precio total")}
								currency="U$S"
							/>
						</div>
						/*
						<ChartLine
							className={"graphic_avg_price"}
							labels={chartPrice.map(o => o.label)}
							datasets={[
								{
									name: t("Precio por m²"),
									values: chartPrice.map(o => o.value),
									colors: chartPriceM2.map(o =>
										o.id == id
											? theme.colors.primaryColor
											: theme.colors.secondaryColor
									),
								},
							]}
							stroke={{ width: 4, color: theme.colors.secondaryColor }}
							height={250}
							trendline={{ label: "Promedio", value: bigData?.avg_price }}
						/>
						*/
					)}
				</Col>
			</Row>
		</>
	);
}
