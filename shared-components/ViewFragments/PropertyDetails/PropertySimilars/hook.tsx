import { gql, useQuery, DocumentNode } from '@apollo/client';
import { FRAGMENT_PROPERTY_CARD } from '../../../Components/Property/PropertyCard/PropertyCard.hook';
import { FRAGMENT_MAP } from '../../../Components/Property/PropertyMap/hook';
import { FRAGMENT_PROPERTY_SIMILARS_CHARTS } from './PropertySimilarsCharts/hook';
import { capitalize } from '../../../Utils/Functions';
import { PropComponentMode } from '../../../Components/Property/PropertyInterfaces';
import { useEffect } from 'react';

interface usePropertySimilarsProps {
	property_id: string;
	results_per_page?: number;
	page?: number;
	mode?: PropComponentMode;
}

const QUERY_RELATED = gql`
	query similar($id: Int!, $isProject: Boolean, $results_per_page: Int!, $page: Int) {
		similarById(id: $id, isProject: $isProject, first: $results_per_page, page: $page) {
		data {
			__typename
			id
			${FRAGMENT_PROPERTY_CARD.query()}
			${FRAGMENT_MAP.query()}
			${FRAGMENT_PROPERTY_SIMILARS_CHARTS.query()}
		}
	}
}`;

export const usePropertySimilars = ({
	property_id,
	results_per_page = 20,
	page = 1,
	mode = 'property',
}: usePropertySimilarsProps) => {
	const { loading, data, error } = useQuery(QUERY_RELATED, {
		variables: {
			id: property_id,
			results_per_page,
			page,
			isProject: mode === 'project',
		},
		skip: property_id == undefined
	});

	return {
		loading,
		data: data?.similarById.data,
		error,
	};
};
