import { gql } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { useFilters } from "../../../Components/Filters/Filters.hook";

export const QUERY_DESTACADO_HOME = gql`
	query DestacadoHome($operation_type_id: Int) {
		featuredBannerHome(operationType: $operation_type_id) {
			active
			created_at
			deleted_at
			url
			html_logo
			id
			image
			image_vertical
			logo
			logo_bg_color
			logo_mobile
			logo_mobile_width
			logo_text_color
			operation_type_id
			logo_width
			name
			probability
		}
	}
`;

const DEFAULT_PROPS = {
	fetchPolicy: "cache-first",
};

const useBannerHome = props => {
	props = { ...DEFAULT_PROPS, ...props };
	const {
		filters: { operation_type_id },
	} = useFilters();

	const { data, loading, error } = useQuery(QUERY_DESTACADO_HOME, {
		variables: { operation_type_id: operation_type_id },
		fetchPolicy: props.fetchPolicy,
	});

	return {
		loading: loading,
		data: data?.featuredBannerHome,
		error: error,
	};
};

export { useBannerHome };
