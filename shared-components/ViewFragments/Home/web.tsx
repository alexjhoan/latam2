import "./styles.less";

import { Col, Row, Typography } from "antd";

import { BannerFeaturedAgents } from "./BannerFeaturedAgents/web";
import { BannerHome } from "./BannerHome/web";
import { BannerSubHome } from "./BannerSubHome/web";
import { BrandInfo } from "./BrandInfo/web";
import { FeaturedSearch } from "./FeaturedSearch/web";
import { Footer } from "../Footer/web";
import { H1 } from "../../Components/SEO/web";
import Header from "../Header/web";
import { HomeFilters } from "./HomeFilters/web";
import { NewsSection } from "./NewsSection/web";
import { PicHomeFooter } from "../Footer/PicHomeFooter/web";
import { useTheme } from "../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Title } = Typography;

export const HomeLayout = () => {
	const { t } = useTranslation();
	const { theme } = useTheme();

	return (
		<>
			{/* Header */}
			<Header showSearchBar={false} />

			{/* Main */}
			<Row
				className="home-container"
				gutter={[
					0,
					{
						xs: theme.spacing.xxlSpacing * 2,
						sm: theme.spacing.xxlSpacing * 2,
						md: theme.spacing.smSpacing * 6,
					},
				]}>
				<Col span={24}>
					<div className="home-top-container">
						<div className="banner-home-container">
							{/* Banner Homer */}
							<BannerHome />
							<div className={"home-search-box-container"}>
								<Row
									justify={"center"}
									gutter={[theme.spacing.lgSpacing, theme.spacing.lgSpacing]}>
									{/* Home h1 */}
									<Col span={24} style={{ textAlign: "center" }}>
										<H1 />
									</Col>
									{/* Home Filters */}
									<Col span={24}>
										<HomeFilters />
									</Col>
								</Row>
							</div>
						</div>

						{/* BannerFeaturedAgents */}
						<div className={"home-featured-agents"}>
							<div className="container">
								<BannerFeaturedAgents />
							</div>
						</div>
					</div>
				</Col>

				{/* Banner SubHome */}
				<Col span={24} className={"home-section-container banner-subhome-container"}>
					<div className="container">
						<BannerSubHome />
					</div>
				</Col>

				{/* BrandInfo */}
				<Col span={24} className={"home-section-container brand-info-container"}>
					<div className="container">
						<Title className="home-section-title" level={1}>
							{t("Mirá como InfoCasas te puede ayudar")}
						</Title>
						<BrandInfo />
					</div>
				</Col>

				{/* FeaturedSearch */}
				<Col span={24} className={"home-section-container featured-search-container"}>
					<div className="container">
						<Title level={1} className="home-section-title">
							{t("Búsquedas Recomendadas")}
						</Title>
					</div>
					<FeaturedSearch />
				</Col>

				{/* NewsSection */}
				<Col span={24} className={"home-section-container news-section-container"}>
					<Title className="home-section-title" level={1}>
						{t("Noticias")}
					</Title>
					<div className="container">
						<NewsSection />
					</div>
				</Col>
			</Row>

			{/* Footer */}
			<Footer size={"large"} />

			{/* Footer Image */}
			<PicHomeFooter />

			<style jsx>{`
				.home-top-container {
					min-height: calc(100vh - ${theme.headerHeight}px);
					transition: min-height 0.4s ease;
				}
				.home-top-container .banner-home-container {
					width: calc(100% - ${theme.spacing.lgSpacing * 2}px);
					border-radius: ${theme.spacing.mdSpacing}px;
				}
				.home-section-container .home-section-title {
					margin-bottom: ${theme.spacing.xxlSpacing}px;
				}
				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.home-top-container .banner-home-container .home-search-box-container {
						width: calc(100% - ${theme.spacing.mdSpacing * 2}px);
					}
				}

				.home-top-container .banner-home-container {
					background-color: ${theme.colors.backgroundColorAternative};
				}

				.home-top-container .banner-home-container .home-search-box-container :global(h1) {
					color: ${theme.colors.backgroundColor};
				}

				.home-top-container .home-featured-agents {
					padding: ${theme.spacing.lgSpacing}px 0;
				}
			`}</style>
		</>
	);
};
