import "./styles.less";

import { Button, Skeleton, Typography } from "antd";

import { ImagenOptimizada as Img } from "../../../../Components/Image/web";
import { RightOutlined } from "@ant-design/icons";
import { removeUrlProtocol } from "../../../../Utils/Functions";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { Text, Link } = Typography;

export const FeaturedSearchItem = ({ item, loading }) => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const splitText = item.featured.title.split(" ");
	const operationText = splitText.shift();
	const restText = splitText.join(" ");

	return (
		<>
			<Link href={loading ? null : removeUrlProtocol(item.featured.url)} target="_blank">
				<div className={"item"}>
					{loading ? (
						<Skeleton.Image />
					) : (
						<>
							<Img src={item.featured.image} alt={item.featured.title} />
							<div className="overLay" />
							<Text className="operation-text">{operationText}</Text>
							<Text className="text">{restText}</Text>
							<Button className="more" type="text">
								{t("Ver más")}
								<RightOutlined />
							</Button>
						</>
					)}
				</div>
			</Link>

			<style jsx global>{`
				.item {
					background-color: ${theme.colors.backgroundColorAternative};
					border-radius: ${theme.spacing.smSpacing}px;
				}

				.item .operation-text {
					color: ${theme.colors.backgroundColor};
					padding: ${theme.spacing.lgSpacing}px;
					font-size: ${theme.fontSizes.mdTitle};
				}

				.item .text {
					color: ${theme.colors.backgroundColor};
					padding: ${theme.spacing.lgSpacing}px;
				}

				.item .more {
					color: ${theme.colors.backgroundColor};
					padding: 0 ${theme.spacing.lgSpacing}px;
				}
			`}</style>
		</>
	);
};
