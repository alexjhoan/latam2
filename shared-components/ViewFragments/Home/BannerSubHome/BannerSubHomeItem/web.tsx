import { Skeleton, Typography } from "antd";

import { ImagenOptimizada as Img } from "../../../../Components/Image/web";

const { Link } = Typography;

export const BannerSubHomeItem = ({ image, link, position, key, loading }) => {
	return (
		<>
			<div className="destacado" key={key ?? ''}>
				{loading ? (
					<Skeleton.Image key={"banner-sub-home" + key} className="image-container" />
				) : (
					<Link className="image-container" href={link} target="_blank">
						<Img src={image} alt={image} />
					</Link>
				)}
			</div>

			<style jsx>{`
				@keyframes slideFromRight {
					0% {
						transform: translateX(75%);
						opacity: 0;
					}
					100% {
						transform: translateX(0);
						opacity: 1;
					}
				}
				.destacado {
					animation: slideFromRight 900ms ease ${400 * position}ms 1 forwards;
				}
			`}</style>
		</>
	);
};
