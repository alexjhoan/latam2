import "./style.less";

import React, { useEffect, useState } from "react";

import { BannerSubHomeItem } from "./BannerSubHomeItem/web";
import { Carousel } from "antd";
import { useBannerSubHome } from "./BannerSubHome.hook";
import { useTheme } from "../../../Styles/ThemeHook";

export const BannerSubHome = () => {
	const { theme } = useTheme();
	const { data, loading, error } = useBannerSubHome({ limit: 3 });

	const [settings, setSettings] = useState({
		render: true,
		arrows: false,
		responsive: [],
		infinite: true,
		speed: 400,
		autoplaySpeed: 6500,
		autoplay: false,
		slidesToShow: 3,
		slidesToScroll: 1,
	});

	useEffect(() => {
		setSettings({
			...settings,
			render: false,
			responsive: [
				{
					breakpoint: Number(
						theme.breakPoints.md.substring(0, theme.breakPoints.md.length - 2)
					),
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						dots: true,
						autoplay: true,
						autoplaySpeed: 6800,
					},
				},
				{
					breakpoint: Number(
						theme.breakPoints.sm.substring(0, theme.breakPoints.sm.length - 2)
					),
					settings: {
						slidesToShow: 2,
						slidesToScroll: 1,
						dots: true,
						autoplay: true,
						autoplaySpeed: 6800,
					},
				},
				{
					breakpoint: Number(
						theme.breakPoints.xs.substring(0, theme.breakPoints.xs.length - 2)
					),
					settings: {
						slidesToShow: 1,
						slidesToScroll: 1,
						dots: true,
						autoplay: true,
						autoplaySpeed: 6800,
					},
				},
			],
		});
	}, []);

	useEffect(() => {
		if (!settings.render) setSettings({ ...settings, render: true });
	}, [settings.render]);

	if (!settings.render) return null;
	if (error) return null;
	if (data && data.length < 3) return null;

	const banners = loading ? [...Array(3)] : data;

	return (
		<>
			<div className="banner-sub-home">
				<Carousel dots={{ className: "banner-sub-home-dots" }} {...settings}>
					{banners.map((banner, i) => (
						<BannerSubHomeItem
							key={"banner_subhome_" + i}
							image={banner?.image}
							link={banner?.url}
							position={i + 1}
							loading={loading}
						/>
					))}
				</Carousel>
			</div>
			<style jsx global>{`
				.banner-sub-home .slick-list .destacado {
					width: calc(100% - ${theme.spacing.xxlSpacing}px);
					border-radius: ${theme.spacing.smSpacing}px;
					margin-left: ${theme.spacing.lgSpacing}px;
					margin-right: ${theme.spacing.lgSpacing}px;
				}

				@media screen and (max-width: ${theme.breakPoints.lg}) {
					.banner-sub-home .slick-list .destacado {
						width: calc(100% - ${theme.spacing.lgSpacing}px);
						margin-left: ${theme.spacing.smSpacing}px;
						margin-right: ${theme.spacing.smSpacing}px;
					}
				}

				@media screen and (max-width: ${theme.breakPoints.xl}) {
					.ant-carousel .slick-dots-bottom li.slick-active button {
						background: ${theme.colors.textColor};
					}

					.ant-carousel .slick-dots-bottom li button {
						background: ${theme.colors.textInverseColor};
					}

					.ant-carousel .slick-dots-bottom li button:before {
						font-size: ${theme.spacing.mdSpacing + 1}px;
					}

					.banner-sub-home.slick-list .destacado {
						margin: 0px ${theme.spacing.smSpacing}px;
						width: calc(100% - ${theme.spacing.lgSpacing}px);
					}
				}
			`}</style>
		</>
	);
};
