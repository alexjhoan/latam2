import React, { useState } from "react";
import { gql } from "@apollo/client";
import { useQuery } from "@apollo/client";
import { FRAGMENT_OPERATIONTYPES } from "../../../../Components/Filters/OperationType/OperationType.hook";
import { FRAGMENT_PROPERTYTYPES } from "../../../../Components/Filters/PropertyType/PropertyType.hook";
import { FRAGMENT_PROPSTATES } from "../../../../Components/Filters/PropStates/PropStates.hook";
import { FRAGMENT_BATHROOMS } from "../../../../Components/Filters/Bathrooms/Bathrooms.hook";
import { FRAGMENT_BEDROOMS } from "../../../../Components/Filters/Bedrooms/Bedrooms.hook";
import { FRAGMENT_ROOMS } from "../../../../Components/Filters/Rooms/Rooms.hook";
import { FRAGMENT_PUBLICATIONDATES } from "../../../../Components/Filters/PublicationDate/PublicationDate.hook";
import { FRAGMENT_SEADISTANCE } from "../../../../Components/Filters/SeaDistance/SeaDistance.hook";
import { FRAGMENT_FACILITIES } from "../../../../Components/Filters/Facilities/Facilities.hook";
import { FRAGMENT_PRICES_COMMONEXPENSES } from "../../../../Components/Filters/Price/Price.hook";
import { FRAGMENT_ORDERS } from "../../../../Components/Filters/Order/Order.hook";
import { FRAGMENT_SEASONS } from "../../../../Components/Filters/TemporalFilter/Seasons/Seasons.hook";
import { FRAGMENT_GUESTS } from "../../../../Components/Filters/Guests/Guests.hook";
import { FRAGMENT_PRIVATEOWNER } from "../../../../Components/Filters/PrivateOwner/PrivateOwner.hook";
import { FRAGMENT_SOCIALHOUSING } from "../../../../Components/Filters/SocialHousing/SocialHousing.hook";
import { FRAGMENT_DATERANGE } from "../../../../Components/Filters/TemporalFilter/DateRange/DateRange.hook";
import { FRAGMENT_SURFACERANGE } from "../../../../Components/Filters/SurfaceRange/SurfaceRange.hook";
import { FRAGMENT_DISPOSITION } from "../../../../Components/Filters/Disposition/Disposition.hook";
import { FRAGMENT_FLOORS } from "../../../../Components/Filters/Floors/Floors.hook";
import { useFilters } from "../../../../Components/Filters/Filters.hook";
import { encodeHashUrl } from "../../../../Utils/Functions";
import { useRouter } from "next/router";

const QUERY_AVAILABLE_FILTERS = gql`
  query availableFilters {
    availableFilters{
        ${FRAGMENT_OPERATIONTYPES.query()}
        ${FRAGMENT_PROPERTYTYPES.query()}
        ${FRAGMENT_PROPSTATES.query()}
        ${FRAGMENT_BATHROOMS.query()}
        ${FRAGMENT_BEDROOMS.query()}
        ${FRAGMENT_ROOMS.query()}
        ${FRAGMENT_PUBLICATIONDATES.query()}
        ${FRAGMENT_SEADISTANCE.query()}
        ${FRAGMENT_FACILITIES.query()}
        ${FRAGMENT_PRICES_COMMONEXPENSES.query()}
        ${FRAGMENT_ORDERS.query()}
        ${FRAGMENT_SEASONS.query()}
        ${FRAGMENT_GUESTS.query()}
        ${FRAGMENT_PRIVATEOWNER.query()}
        ${FRAGMENT_SURFACERANGE.query()}
        ${FRAGMENT_SOCIALHOUSING.query()}
        ${FRAGMENT_DATERANGE.query()}
        ${FRAGMENT_DISPOSITION.query()}
        ${FRAGMENT_FLOORS.query()}
    }
  }
  `;

const QUERY_SEARCH_URL = gql`
	query searchUrl($params: SearchParamsInput!) {
		searchUrl(params: $params) {
			url
		}
	}
`;

const useSearchResultsFilters = (forceUrlUpdate = false) => {
	const { filters, changeFilters, filtersTags } = useFilters();
	const [updateURL, setUpdateURL] = useState(false);
	const router = useRouter();

	useQuery(QUERY_SEARCH_URL, {
		variables: {
			params: filters,
		},
		skip: !updateURL && !forceUrlUpdate,
		onCompleted: dataURL => {
			if (typeof window !== "undefined" && window.history) {
				router.push(
					{
						pathname: "/searchPage",
						query: { hashed: encodeHashUrl({ filters: filtersTags }) },
					},
					dataURL.searchUrl.url
				);
			}
		},
	});

	const changeFiltersAndUpdateURL = new_filters => {
		if (!updateURL) setUpdateURL(true);
		changeFilters(new_filters);
	};

	const { data, loading, error } = useQuery(QUERY_AVAILABLE_FILTERS);

	return {
		data,
		loading,
		error,
		changeFiltersAndUpdateURL,
	};
};

export { useSearchResultsFilters };
