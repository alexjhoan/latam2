import React, { useRef, useState } from "react";
import { View, Text, KeyboardAvoidingView } from "react-native";
import { useSearchResultsFilters } from "./SearchResultsFilters.hook";

import { PropertyType } from "../../../../Components/Filters/PropertyType/app";
import { OperationType } from "../../../../Components/Filters/OperationType/app";
import { PropStates } from "../../../../Components/Filters/PropStates/app";
import { Bathrooms } from "../../../../Components/Filters/Bathrooms/app";
import { Bedrooms } from "../../../../Components/Filters/Bedrooms/app";
import { Guests } from "../../../../Components/Filters/Guests/app";
import { ScrollView } from "react-native-gesture-handler";
import { Rooms } from "../../../../Components/Filters/Rooms/app";
import { PublicationDate } from "../../../../Components/Filters/PublicationDate/app";
import { SeaDistance } from "../../../../Components/Filters/SeaDistance/app";
import { PrivateOwner } from "../../../../Components/Filters/PrivateOwner/app";
import { Location } from "../../../../Components/Filters/Location/app";
import t from "../../../../i18n";
// import { Facilities } from "../../../Components/Filters/Facilities/app";
// import { Price } from "../../../Components/Filters/Price/app";
// import { Seasons } from "../../../Components/Filters/Seasons/app";
// import { SocialHousing } from "../../../Components/Filters/SocialHousing/app";
// import { DateRange } from "../../../Components/Filters/DateRange/app";
// import { SurfaceRange } from "../../../Components/Filters/SurfaceRange/app";
// import { Keyword } from "../../../Components/Filters/Keyword/app";
// import { Disposition } from "../../../Components/Filters/Disposition/app";
// import { Floors } from "../../../Components/Filters/Floors/app";

const Filters = () => {
	const [showDateRange, setShowDateRange] = useState(true); // deberia ir en el hook
	const { data, loading, error, changeFilter } = useSearchResultsFilters();

	if (error) return <Text>{t("error")}</Text>;
	if (loading) return <Text>{t("loading")}</Text>;

	return (
		<ScrollView keyboardShouldPersistTaps={"always"}>
			<KeyboardAvoidingView behavior={"padding"}>
				{/* <Text>Filtros</Text>
				{/* <Text>DEBUG_STORED_FILTERS {JSON.stringify(data.storedFilters)}</Text> */}
				<Text>
					DEBUG_STORED_FILTERS_COMPLETE {JSON.stringify(data.storedFiltersTags)}
				</Text>
				<OperationType
					labeled
					inputType="radioselect"
					selectedValue={data.storedFilters["operation_type_id"]}
					filterChanged={changeFilter}
					currentFilters={data.storedFilters}
				/>
				<PropertyType
					labeled
					inputType={"checkboxselect"}
					filterChanged={changeFilter}
					selectedValue={data.storedFilters["property_type_id"]}
					currentFilters={data.storedFilters}
				/>
				<Location
					labeled
					filterChanged={changeFilter}
					currentFilters={data.storedFilters}
					currentFiltersTags={data.storedFiltersTags}
				/>
				<Guests
					labeled
					selectedValue={data.storedFilters["guests"]}
					filterChanged={changeFilter}
					currentFilters={data.storedFilters}
				/>
				<PropStates
					labeled
					selectedValue={data.storedFilters["constStatesID"]}
					filterChanged={changeFilter}
					currentFilters={data.storedFilters}
				/>
				<Bathrooms
					labeled
					selectedValue={data.storedFilters["bathrooms"]}
					filterChanged={changeFilter}
					currentFilters={data.storedFilters}
				/>
				<Bedrooms
					labeled
					selectedValue={data.storedFilters["bedrooms"]}
					filterChanged={changeFilter}
					currentFilters={data.storedFilters}
				/>
				<Rooms
					labeled
					selectedValue={data.storedFilters["rooms"]}
					filterChanged={changeFilter}
					currentFilters={data.storedFilters}
				/>
				<PublicationDate
					labeled
					filterChanged={changeFilter}
					selectedValue={data.storedFilters["publicationDate"]}
					currentFilters={data.storedFilters}
				/>
				<SeaDistance
					labeled
					filterChanged={changeFilter}
					selectedValue={data.storedFilters["seaDistanceID"]}
					currentFilters={data.storedFilters}
				/>
				<PrivateOwner
					filterChanged={changeFilter}
					selectedValue={data.storedFilters["privateOwner"]}
					currentFilters={data.storedFilters}
				/>

				<View style={{ height: 60 }}></View>
			</KeyboardAvoidingView>
		</ScrollView>
	);
};

export { Filters };
