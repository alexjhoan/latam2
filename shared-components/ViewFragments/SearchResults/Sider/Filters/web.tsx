import "./styles.less";

import { Bathrooms } from "../../../../Components/Filters/Bathrooms/web";
import { Bedrooms } from "../../../../Components/Filters/Bedrooms/web";
import { Disposition } from "../../../../Components/Filters/Disposition/web";
import { Facilities } from "../../../../Components/Filters/Facilities/web";
import { Floors } from "../../../../Components/Filters/Floors/web";
import { Guests } from "../../../../Components/Filters/Guests/web";
import { Keyword } from "../../../../Components/Filters/Keyword/web";
import { Location } from "../../../../Components/Filters/Location/web";
import { OperationType } from "../../../../Components/Filters/OperationType/web";
import { Price } from "../../../../Components/Filters/Price/web";
import { PrivateOwner } from "../../../../Components/Filters/PrivateOwner/web";
import { PropStates } from "../../../../Components/Filters/PropStates/web";
import { PropertyType } from "../../../../Components/Filters/PropertyType/web";
import { PublicationDate } from "../../../../Components/Filters/PublicationDate/web";
import React from "react";
import { Rooms } from "../../../../Components/Filters/Rooms/web";
import { SeaDistance } from "../../../../Components/Filters/SeaDistance/web";
import { SocialHousing } from "../../../../Components/Filters/SocialHousing/web";
import { SurfaceRange } from "../../../../Components/Filters/SurfaceRange/web";
import { TemporalFilter } from "../../../../Components/Filters/TemporalFilter/web";
import { useSearchResultsFilters } from "./SearchResultsFilters.hook";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const Filters = () => {
	const { error, changeFiltersAndUpdateURL } = useSearchResultsFilters();
	if (error) return null;

	return (<>
		<div className="search-page-filters">
			<OperationType
				collapsable
				inputType={"radioselect"}
				filterChanged={changeFiltersAndUpdateURL}
			/>
			<PropertyType
				collapsable
				inputType={"checkboxselect"}
				filterChanged={changeFiltersAndUpdateURL}
			/>
			<Price collapsable filterChanged={changeFiltersAndUpdateURL} />
			<Location collapsable filterChanged={changeFiltersAndUpdateURL} />
			<Keyword collapsable filterChanged={changeFiltersAndUpdateURL} />
			<TemporalFilter collapsable filterChanged={changeFiltersAndUpdateURL} />
			<SurfaceRange collapsable filterChanged={changeFiltersAndUpdateURL} />
			<PropStates collapsable filterChanged={changeFiltersAndUpdateURL} />
			<Guests collapsable filterChanged={changeFiltersAndUpdateURL} />
			<Bedrooms collapsable filterChanged={changeFiltersAndUpdateURL} />
			<Rooms collapsable filterChanged={changeFiltersAndUpdateURL} />
			<Bathrooms collapsable filterChanged={changeFiltersAndUpdateURL} />
			<Floors collapsable filterChanged={changeFiltersAndUpdateURL} />
			<PublicationDate collapsable filterChanged={changeFiltersAndUpdateURL} />
			<SeaDistance collapsable filterChanged={changeFiltersAndUpdateURL} />
			<Disposition collapsable filterChanged={changeFiltersAndUpdateURL} />
			<SocialHousing filterChanged={changeFiltersAndUpdateURL} />
			<PrivateOwner filterChanged={changeFiltersAndUpdateURL} />
			<Facilities
				collapsable
				inputType={"select"}
				filterChanged={changeFiltersAndUpdateURL}
			/>
		</div>
	</>);
};

export { Filters };
