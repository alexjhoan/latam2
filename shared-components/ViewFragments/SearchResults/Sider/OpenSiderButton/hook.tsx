import { useFilters } from "../../../../Components/Filters/Filters.hook";

export const useOpenSiderButton = () => {
	const { filters } = useFilters();

	let count: number = Object.values(filters).filter(
		e => (!Array.isArray(e) && e != null) || (Array.isArray(e) && e.length > 0)
	).length;
	count = count - 3; // resto pagina,tipo de operacion y order que siempre van seteados

	return {
		count: count,
	};
};
