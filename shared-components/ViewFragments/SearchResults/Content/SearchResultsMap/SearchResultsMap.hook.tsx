import { useState, useEffect } from "react";
import { LatLng, PointInPolygon, Polygon, SimplifyPolygon } from "../../../../Utils/Polygons";
import { useQuery } from "@apollo/client";
import { gql } from "@apollo/client";
import { FRAGMENT_PRICETAG } from "../../../../Components/Property/PriceTag/PriceTag.hook";
import { useMarkSeen } from "../../../../Components/Property/SeenStatus/SeenStatus.hook";
import { useDebouncedCallback } from "../../../../GlobalHooks/useDebounce";
import { useFilters } from "../../../../Components/Filters/Filters.hook";
import { useAuthCheck } from "../../../../Components/User/useAuthCheck";
import { FRAGMENT_PROPERTY_CARD } from "../../../../Components/Property/PropertyCard/PropertyCard.hook";

const markerToLatLng = (m): LatLng => {
	return {
		...m,
		latitude: parseFloat(m.latitude),
		longitude: parseFloat(m.longitude),
	};
};

const QUERY_NEIGHBORHOOD_POLYGON = gql`
	query NeighborhoodPolygon($id: Int!) {
		location(id: $id, type: Neighborhood) {
			__typename
			... on Neighborhood {
				id
				name
				lat
      			long
				polygon {
					latitude
					longitude
					__typename
				}
			}
		}
	}
`;

const QUERY_SEARCH_RESULTS_MAP = gql`
    query ResultsMap($rows: Int!, $params: SearchParamsInput!, $page: Int) {
        properties: searchListing(params: $params, first: $rows, page: $page) {
            data {
               __typename
               id
               latitude
               longitude
                ${FRAGMENT_PRICETAG.query()}
								${FRAGMENT_PROPERTY_CARD.query()}
            }
             paginatorInfo {
                lastPage
                firstItem
                lastItem
                total
             }
        }
    }
`;

const DEFAULT_MAP_CENTER = { lat: -34.9030204, lng: -56.1494064 };

const useSearchResultsMapWithBackend = (max_points_to_show: Number) => {
	const [activeMarkers, setActiveMarkers] = useState([]);
	const [totalMarkers, setTotalMarkers] = useState(0);
	const { filters, changeFilters } = useFilters();
	const [mapCenter, setMapCenter] = useState<any>();

	useQuery(QUERY_NEIGHBORHOOD_POLYGON, {
		variables: {
			id:
				filters && filters.neighborhood_id && filters.neighborhood_id.length == 1
					? filters.neighborhood_id[0]
					: null,
		},
		skip: !(
			filters &&
			filters.neighborhood_id &&
			filters.neighborhood_id.length == 1 
			// @todo - check if this can be commented
			//&&
			//filters.map_polygon &&
			//filters.map_polygon.length == 0
		),
		onCompleted: dataNeighborhoodPolygon => {
			// set polygon
			if (
				dataNeighborhoodPolygon &&
				dataNeighborhoodPolygon.location &&
				dataNeighborhoodPolygon.location.polygon.length > 1
			) {
				changeFilters({
					map_polygon: {
						text: dataNeighborhoodPolygon.location.name,
						value: dataNeighborhoodPolygon.location.polygon,
					},
				});
			}
			// set map center
			if(dataNeighborhoodPolygon.location?.lat && dataNeighborhoodPolygon.location?.long){
				setMapCenter({
					lat: dataNeighborhoodPolygon.location?.lat,
					lng: dataNeighborhoodPolygon.location?.long
				})
			}else { setMapCenter(DEFAULT_MAP_CENTER); }
		},
	});

	const [selectedMarker, setSelectedMarker] = useState(0);

	const storedFiltersForMap = f => {
		const filters = f == null ? {} : { ...f };
		if (filters && filters["map_polygon"] && filters["map_polygon"].length > 0) {
			filters["estate_id"] = null;
			filters["neighborhood_id"] = [];
		}
		return filters;
	};

	const { data, loading, error } = useQuery(QUERY_SEARCH_RESULTS_MAP, {
		variables: {
			rows: max_points_to_show,
			params: storedFiltersForMap(filters),
			page: 0,
		},
		skip: !(
			filters &&
			filters.operation_type_id &&
			filters.map_bounds &&
			filters.map_bounds.length > 0
		),
	});
	const { markSeen } = useMarkSeen();
	const { isLoggedIn } = useAuthCheck();

	
	useEffect(() => {
		// set map center
		if(
			!(
				filters &&
				filters.neighborhood_id &&
				filters.neighborhood_id.length == 1 
				// @todo - check if this can be commented
				//&&
				//filters.map_polygon &&
				//filters.map_polygon.length == 0
			)
		){ setMapCenter(DEFAULT_MAP_CENTER); }

		// Delete mapBounds when map exit
		return () => {
			changeFilters({ map_bounds: null, map_polygon: null});
		}
	}, []);

	useEffect(() => {
		if(data){
			setActiveMarkers(data.properties.data);
			setTotalMarkers(data.properties.paginatorInfo.total);
		}
	}, [data])

	const mapRegionChangedDo = (bounds: Polygon, zoomLevel: Number) => {
		changeFilters({ map_bounds: { text: "Map Bounds", value: bounds } });
	};
	const [mapRegionChanged] = useDebouncedCallback(mapRegionChangedDo, 1000);

	const setPolygon = polygon => {
		if (polygon != null && polygon.length > 2) {
			const p = SimplifyPolygon(polygon, 0.0004, false);
			changeFilters({
				map_polygon: { text: "Map Polygon", value: [...p, p[0]] },
			});
		} else {
			changeFilters({ map_polygon: null });
		}
	};

	const selectMarker = propertyId => {
		if (isLoggedIn && propertyId > 0) {
			markSeen(propertyId);
		}
		setSelectedMarker(propertyId);
	};

	return {
		//QUERY DATA
		loading,
		error,

		//COMPONENT DATA
		mapRegionChanged,
		markers: {
			activeMarkers,
			totalMarkers
		},

		polygon: filters && filters.map_polygon ? filters.map_polygon : [],
		mapCenter: mapCenter ?? null,
		setPolygon,

		selectedMarker,
		selectMarker,
	};
};

export { useSearchResultsMapWithBackend };
