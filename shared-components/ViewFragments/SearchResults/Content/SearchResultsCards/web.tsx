import { Col, Empty, Grid, List, Row } from "antd";
import {
	PropertyCard,
	PropertyCardSizes,
	PropertyCardSkeleton,
} from "../../../../Components/Property/PropertyCard/web";
import React, { useEffect } from "react";

import { BigDataSearchResults } from "../../../../Components/BigData/SearchResult/web";
import { ColProps } from "antd/lib/grid/col";
import { RTBhouse } from "../../../../Components/RTBhouse/web";
import { SearchResultDisplay } from "./SearchResultsPagination/ResultDisplay/web";
import { SearchResultsPagination } from "./SearchResultsPagination/web";
import { isSuperHighlighted } from "../../../../Utils/Functions";
import { useGoogleTagManager } from "../../../../GlobalHooks/web/GoogleTagManager.hook";
import { useSearchResultsCards } from "./SearchResultsCards.hook";
import { useTheme } from "../../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { useBreakpoint } = Grid;

export function SearchResultsCards() {
	const { loading, data, paginatorInfo } = useSearchResultsCards();
	const { t } = useTranslation();
	const { theme } = useTheme();
	const screen = useBreakpoint();
	const GTM = useGoogleTagManager();

	useEffect(() => {
		if (data && data.length > 0) {
			GTM.Event({
				name: "searchresults",
				data: {
					listing_id: data.map(p => p.id),
					listing_totalvalue: data.map(p => p.price.amount),
					listing_pagetype: "searchresults",
				},
			});
		}
	}, [data]);

	if (loading || !data) {
		return (
			<Col span={24}>
				<List
					itemLayout={"horizontal"}
					dataSource={[...Array(6)].map((o, i) => i)}
					grid={{
						gutter: theme.spacing.lgSpacing,
						column: 3,
						xs: 1,
						sm: 2,
						lg: 2,
						xxl: 3,
					}}
					renderItem={o => (
						<List.Item>
							<PropertyCardSkeleton />
						</List.Item>
					)}
				/>
			</Col>
		);
	}
	return (
		<>
			{data.length == 0 ? (
				<Col span={24}>
					<Empty
						style={{ padding: "50px 0px" }}
						description={t("No se encontraron resultados para su busqueda.")}
					/>
				</Col>
			) : (
				<>
					<Col span={24}>
						<Row
							align={"top"}
							gutter={[theme.spacing.lgSpacing, theme.spacing.lgSpacing]}>
							{data.map(property => {
								let grid: ColProps = {
									xs: 24,
									sm: 12,
									md: 8,
									lg: 12,
									xl: 8,
									xxl: 8,
								};
								let size: PropertyCardSizes = "default";

								const superHighlight = isSuperHighlighted(property.highlight);
								if (superHighlight) {
									grid = { span: 24 };
									size = "large";
								}

								return property ? (
									<Col key={"property_" + property.id} {...grid}>
										<PropertyCard
											id={property.id}
											size={size}
											hideActions={screen.sm}
										/>
									</Col>
								) : (
									<></>
								);
							})}
						</Row>
						<Row
							justify={"center"}
							style={{ paddingTop: `${theme.spacing.lgSpacing}px` }}>
							<Col>
								<SearchResultsPagination />
							</Col>
						</Row>
						<Row justify={"center"}>
							<Col className="result-display-footer">
								<SearchResultDisplay />
							</Col>
						</Row>
					</Col>
					<Col span={24}>
						<BigDataSearchResults />
					</Col>
				</>
			)}

			<RTBhouse page={"listing"} data={data.map(p => p.id)} />
			<style jsx global>{`
				.result-display-footer {
					margin-top: ${theme.spacing.smSpacing}px;
				}

				.result-display-footer .search-result-display {
					font-size: ${theme.fontSizes.xsFontSize} !important;
					color: ${theme.colors.textSecondaryColor};
				}

				.result-display-footer .search-result-display .ant-typography {
					color: ${theme.colors.textSecondaryColor};
				}
			`}</style>
		</>
	);
}
