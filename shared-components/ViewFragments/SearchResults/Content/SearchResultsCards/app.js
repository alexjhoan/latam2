import React from "react";
import { useSearchResultsCards } from "./SearchResultsCards.hook";
import { PropertyCard } from "../../../Components/Property/PropertyCard/web";
import { View, Text } from "react-native";
import t from "../../../i18n";

export function SearchResultsCards() {
	const x = useSearchResultsCards();

	if (x.loading || !x.data) {
		return <Text>{t("loading")}</Text>;
	}

	return (
		<View>
			{x.data.map(o => {
				return <PropertyCard key={o.id} id={o.id} />;
			})}
		</View>
	);
}
