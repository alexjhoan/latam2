import "./styles.less";

import { Col, Layout, Row } from "antd";

import { LinkBox } from "../../Components/SEO/web";
import { MoreAboutUs } from "./MoreAboutUs/web";
import { SocialNetworks } from "./SocialNetworks/web";
import { useTheme } from "../../Styles/ThemeHook";

const { Footer: FooterComponent } = Layout;

const Footer = ({ size = "default" }: { size?: "large" | "default" }) => {
	const { theme } = useTheme();

	return (
		<>
			<FooterComponent className="footer">
				<Row gutter={[theme.spacing.xxlSpacing, theme.spacing.xxlSpacing]}>
					{size == "large" && (
						<Col span={24} lg={4}>
							<MoreAboutUs />
						</Col>
					)}
					<Col span={24} lg={size == "large" ? 17 : 24}>
						<LinkBox />
					</Col>
					{size == "large" && (
						<Col span={24} lg={3}>
							<SocialNetworks />
						</Col>
					)}
				</Row>
			</FooterComponent>
			<style jsx global>{`
				.footer {
					font-size: ${theme.fontSizes.xsFontSize};
					padding-top: ${theme.spacing.xxlSpacing}px;
				}

				.footer h4.ant-typography {
					font-size: ${theme.fontSizes.baseFontSize};
					margin-bottom: ${theme.spacing.lgSpacing}px;
				}

				.footer a.ant-typography {
					color: ${theme.colors.textColor};
				}

				.footer a.ant-typography:hover {
					color: ${theme.colors.secondaryColor};
				}

				.footer .ant-btn-text {
					color: ${theme.colors.secondaryColor};
					font-size: ${theme.fontSizes.xsFontSize};
				}

				@media screen and (max-width: ${theme.breakPoints.md}) {
					.footer {
						padding-top: ${theme.spacing.xlSpacing}px;
					}
				}
			`}</style>
		</>
	);
};

export { Footer };
