import { Col, Grid, Row, Space, Typography } from "antd";
import {
	FacebookFilled,
	InstagramFilled,
	LinkedinFilled,
	TwitterSquareFilled,
	YoutubeFilled,
} from "@ant-design/icons";

import { removeUrlProtocol } from "../../../Utils/Functions";
import { useSocialNetworks } from "./SocialNetworks.hook";
import { useTheme } from "../../../Styles/ThemeHook";
import { useTranslation } from "react-i18next";

const { useBreakpoint } = Grid;
const { Title, Text, Link } = Typography;

const socialMediaIcons = Object.freeze({
	YouTube: <YoutubeFilled />,
	Facebook: <FacebookFilled />,
	LinkedIn: <LinkedinFilled />,
	Instagram: <InstagramFilled />,
	Twitter: <TwitterSquareFilled />,
});

export const SocialNetworks = () => {
	const { theme } = useTheme();
	const { t } = useTranslation();
	const screens = useBreakpoint();
	const socialMediaLinks = useSocialNetworks();

	return (
		<div className="social-networks">
			<Title level={4}>{t("Seguinos")}</Title>
			<Row gutter={[theme.spacing.xsSpacing, { lg: theme.spacing.xsSpacing }]}>
				{socialMediaLinks?.map(({ url, name }, i) => (
					<Col lg={24} key={"link_box_item_" + i}>
						<Space size={6}>
							{!screens["lg"] && i > 0 && <Text strong>᛫</Text>}
							{screens["lg"] && socialMediaIcons[name]}
							<Link href={removeUrlProtocol(url)} target="_blank" title={name}>
								{t(name)}
							</Link>
						</Space>
					</Col>
				))}
			</Row>
		</div>
	);
};
