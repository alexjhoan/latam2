import "./styles.less";

import { Button, Col, Row } from "antd";
import {
	KeywordLocation,
	setRecientLocations,
} from "../../../Components/Filters/KeywordLocation/web";
import React, { useEffect } from "react";

import { SearchOutlined } from "@ant-design/icons";
import { encodeHashUrl } from "../../../Utils/Functions";
import { useHomeFilters } from "../../Home/HomeFilters/HomeFilters.hook";
import { useRouter } from "next/router";
import { useTheme } from "../../../Styles/ThemeHook";

export const SearchBar = ({ keywordLocationClassName }) => {
	const { theme } = useTheme();
	const { filters, changeFilters, filtersTags, search } = useHomeFilters();
	const router = useRouter();

	useEffect(() => {
		if (search.response.data && !search.response.loading) {
			setRecientLocations(filtersTags.neighborhood_id, filtersTags.estate_id);
			router.push(
				{
					pathname: "/searchPage",
					query: { hashed: encodeHashUrl({ filters: filtersTags }) },
				},
				search.response.data.searchUrl.url
			);
		}
	}, [search.response]);

	useEffect(() => {
		if (router.pathname !== "/searchPage") {
			doSearch();
		}
	}, [filters.estate_id, filters.searchstring]);

	const doSearch = () => {
		if (filters.estate_id != null || filters.searchstring != null) {
			search.send({ variables: { params: filters } });
		}
	};

	return (
		<>
			<div className="search-bar">
				<Row gutter={0}>
					<Col flex={1}>
						<KeywordLocation
							className={keywordLocationClassName}
							filterChanged={changeFilters}
						/>
						<Button
							className={"keyword-location-button"}
							type="primary"
							loading={search.response.loading}
							onClick={() => doSearch()}
							icon={<SearchOutlined />}
						/>
					</Col>
				</Row>
			</div>
			<style jsx global>{`
				.search-bar .keyword-location .ant-select-selector .ant-input {
					border: 1px solid ${theme.colors.backgroundColorAternative};
					background-color: ${theme.colors.backgroundColorAternative};
					padding: ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing * 10}px
						${theme.spacing.smSpacing}px ${theme.spacing.mdSpacing}px;
				}

				.search-bar .keyword-location .ant-select-selector .ant-input:focus {
					border-color: ${theme.colors.borderColor};
					background-color: ${theme.colors.backgroundColor};
				}

				.search-bar .keyword-location-button {
					border-radius: 0 ${theme.spacing.smSpacing}px ${theme.spacing.smSpacing}px 0;
				}
			`}</style>
		</>
	);
};
